# Auxiliary scripts and tools for self-moving mouse project

This repository contains some auxiliary scripts, tools and 
data files for Self-Moving Mouse project. See also: 
[hardware](https://gitlab.com/self-moving-mouse/self-moving-mouse.hardware),
[firmware](https://gitlab.com/fedorkotov/self-moving-mouse.firmware) and
[documentation](https://gitlab.com/fedorkotov/self-moving-mouse.documentation) repositories.

1. `mouse-tracker` - C program for mouse track recording (see [README](mouse-tracker/README.md) inside mouse-tracker folder)
1. `mouse-tracks` - a collection of mouse tracks recorded 
    using mouse-tracker program on a desktop computer 
    with CROWN CMM-960 mouse.
    - `track{NN}.csv` - raw CSV files recorded by mouse-tracker
    - `interpolated_track{NN}.csv` - tracks converted to uniform time grid with 8ms time step
    - `track{NN}.png` - track plots
    - `track{NN}.h` - track data converted to Arduino-compatible
       C header files (x and y deltas without any encoding)
1. `mouse-converter` - a collection of python scripts and
    [Jupyter notebooks](https://jupyter.org/) used to analyze the tracks and to develop
    the most effective track data compression method to 
    be used in [self-moving mouse firmware](https://gitlab.com/fedorkotov/self-moving-mouse.firmware)
    - `mouse-converter/mouse_track_converter.py` - CLI tool
      for interpolation of tracks to uniform time grid and
      plotting results with [matplotlib](https://matplotlib.org/)
    - `mouse-converter/polargrid` is a collection of classes 
      and tools for working with polar grids. It is part of
      my efforts to create an alternative [DPCM](https://en.wikipedia.org/wiki/Differential_pulse-code_modulation)-like
      algorithm for track data encoding that would 
      encode 2d points sequence (instead of 1d samples in the 
      original DPCM) with a polar grid of 255 points organized
      in octosymmetric polar grid. Capabilities
      of this code are demonstrated in `PolarGridsDemo.ipynb`      
    - `mouse-converter/trackencoders` is a library of
      track data encoders including
      - classic 1d DPCM
      - 2d DPCM-like codec with rectilinear grid
      - 2d DPCM-like codec with polar grid
      - DDPCM - 1d DPCM-like codec that works with finite differences
        of second order (~ accelerations) instead of 
        first order second differences in classic DPCM
      
      This library also contains [basinhopping](https://en.wikipedia.org/wiki/Basin-hopping) (~ enhanced [simulated annealing](https://en.wikipedia.org/wiki/Simulated_annealing)) based diff table optimization code for DPCM and DDPCM.    
      
      Codecs performance is compared in `TrackCodecComparison.ipynb` notebook.
      Difference table optimization turned out to be ineffective.
      It can reduce maximum track coordinate error on each
      step but resulting tracks look ragged and unnatural.      
      Probably should switch to some more complex optimization
      criterion that depends not only on track points distance from true 
      positions but also on curvature error as compared to
      original or some measure of smoothness. 
      2d DPCM results are very much the same 
      (rugged unbelievable lines).

      DDPCM with powers of 2 diff table turned out to produce 
      naturally-looking results and acceptably small errors
      for most tracks. I ended up using it in both rev1 and rev2
      firmware.
    - `mouse-converter/PolarGridSelection.ods` spreadsheet
      contains my attempts of manual polar grid 
      selection for 2d DPCM
    - `mouse-converter/ddpcm_track_encoder.py` - CLI wrapper
      for DDPCM difference table optimization and encoding 
      algorithms from `mouse-converter/trackencoders`. 
      Among other things is capable of producing header files with DDPCM
      encoded track data that can be used in Arduino projects
  1. `chosen-tracks-data` - tracks selected for Self-Moving
      mouse firmware encoded with DDPCM with manually selected 
      differences table.
  1. `rng-test` - code for testing quality of
      random numbers generator generator used in Self-Moving Mouse.
      Data collected from the generator is compared to
      with "bad" data collected from clock 
      (last byte of arduino `millis()`) and "good" data
      produced by NumPy `randint(..)` function
  1.  `led-video-to-binary` - scripts for extracting
      binary signal from blinking status LED video.
      Can be useful for debugging devices that do not have
      any debug IO except status LED and you
      do not have logic analyzer handy or attaching it 
      is too difficult.
      `led-video-to-binary/crop-and-extract-frames.sh`
      crops specified part of video with ffmpeg and exports it as
      png frames sequence into specified folder.
      `led-video-to-binary/plot_average_brightness.py`
      calculates average brightness of each frame and
      plots both "analogue" brightness and 
      "digital" signal with specified 0/1 threshold.

# Legal info

`mouse-tracker` program source code is licensed under [CC-BY-SA v3.0](https://creativecommons.org/licenses/by-sa/3.0/) license
because it is derived from https://stackoverflow.com/ answer

`mouse-tracker/sp800_22_tests` is a git submodule taken from
[here](https://github.com/dj-on-github/sp800_22_tests). This 
code is distributed under [GNU GPL v2.0](https://opensource.org/licenses/GPL-2.0).
This code is used only as a standalone utility and is not linked with or imported from my code so it does not affect my code's license.

`rng-test/firmware/serial-print-random` and `rng-test/firmware/serial-print-millis` are licensed under [GNU GPL v2.0](https://opensource.org/licenses/GPL-2.0) because they a based on Arduino framework.

The rest of this repository contents are available under [MIT](https://opensource.org/licenses/MIT) license.
Mouse tracks data and graphics in `mouse-tracks` and `chosen-tracks-data` folders can be also licensed under [CC-BY-SA v4.0](https://creativecommons.org/licenses/by-sa/4.0/) license at your option.
