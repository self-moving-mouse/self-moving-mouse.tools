#!/usr/bin/env bash

# usage example
# ./crop-and-extract-frames VID_20220108_205836.mp4 frames 0.05 0.03 0.65 0.54

INPUT_VIDEO_PATH=$1
OUTPUT_FOLDER=$2
REL_LEFT=$3
REL_TOP=$4
REL_WIDTH=$5
REL_HEIGHT=$6

mkdir -p "${OUTPUT_FOLDER}"

ffmpeg -i "${INPUT_VIDEO_PATH}" \
       -filter:v "crop=in_w*${REL_WIDTH}:in_h*${REL_HEIGHT}:in_w*${REL_LEFT}:in_h*${REL_TOP}" \
       -an -y "${OUTPUT_FOLDER}/%04d.png"
