#!/usr/bin/env python3

import sys
from pathlib import Path

import numpy as np
import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt


def get_avg_brightness(frames_dir):
  brightness = []

  for frame_path in Path(frames_dir).glob('*.png'):
   frame_number = int(frame_path.stem)
   img = cv2.imread(str(frame_path))
   hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
   _, _, v = cv2.split(hsv)
   avg_brightness = np.mean(v.flatten())
   brightness.append(
    (frame_number,
     avg_brightness))

  return np.array(
    [x[1] for x in
     sorted(brightness, key=lambda x: x[0])])



def plot_brightness(brightness, threshold, out_f_path):
  fig, (ax1, ax2) = plt.subplots(2,1)
  fig.set_size_inches(25, 5)

  ax1.plot(brightness)
  ax1.axhline(
    y=threshold,
    color='r', linestyle='dotted', linewidth=2)
  ax1.set_ylabel('brightness (0..255)')
  ax1.set_xlabel('frame number')

  ax2.plot(brightness>threshold)
  ax2.set_ylabel('level')
  ax2.set_xlabel('frame number')

  fig.tight_layout()
  fig.savefig(out_f_path, bbox_inches='tight')

def crop_brightness(brightness, idx_start, idx_end):
  if idx_start<=0:
    idx_start = None
  if idx_end<=0:
    idx_end = None

  if idx_start:
   if idx_end:
     return brightness[idx_start-1:idx_end]
   else:
     return brightness[idx_start-1:]
  else:
   if idx_end:
    return brightness[:idx_end]
   else:
    return brightness


frames_folder_path = sys.argv[1]
threshold = int(sys.argv[2])
plot_path = sys.argv[3]
# optional 1-based frame numbers
idx_start = int(sys.argv[4]) if len(sys.argv)>=5 else -1
idx_end = int(sys.argv[5]) if len(sys.argv)>=6 else -1


brightness = \
  crop_brightness(
    get_avg_brightness(frames_folder_path),
    idx_start,
    idx_end)

plot_brightness(
  brightness,
  threshold,
  plot_path)





