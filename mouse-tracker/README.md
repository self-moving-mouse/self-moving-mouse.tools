# mouse-tracker - X-Windows mouse track recorder

This is a simple console program that records mouse tracks
(cursor movements and mouse button clicks) under X-Server
and writes them to standard output in CSV format with
the following columns
1. event type
    - `MV` - cursor movement
    - `BTN_D` - mouse button down
    - `BTN_U` - mouse button up
1. event time in milliseconds 
1. button number for BTN_D, BTN_U events or cursor delta x for MV 
event.
1. empty for BTN_D, BTN_U events. cursor delta y for MV event

## Build instructions

Just run `make`. mouse-tracker executable will be placed in current folder.

## Usage example
```shell
./mouse-tracker | tee track.csv
```

example output
```
MV, 2827647, 2375, 860
MV, 2828951, 2374, 861
MV, 2828975, 2374, 861
MV, 2829063, 2375, 862
MV, 2829327, 2375, 862
MV, 2829359, 2374, 862
MV, 2829367, 2374, 863
BTN_D, 2830023, 1,
BTN_U, 2830143, 1,
MV, 2830191, 2374, 862
MV, 2831215, 2374, 862
MV, 2831231, 2375, 863
BTN_D, 2831719, 3,
BTN_U, 2831815, 3,
MV, 2833783, 2375, 864
MV, 2833807, 2375, 864
```


## Legal info

mouse-tracker code is a slightly modified version of the source code 
published on [stackoverflow.com](https://stackoverflow.com/) between
2011-04-08 and 2018-05-02. As such it has to be licensed under 
Creative Commons [CC-BY-SA v3.0](https://creativecommons.org/licenses/by-sa/3.0/) license.
See https://meta.stackoverflow.com/a/359368 and https://stackoverflow.com/help/licensing
