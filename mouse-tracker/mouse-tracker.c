#include <stdio.h>
#include <X11/Xlib.h>

// slightly modified version of code
// from this stackoverflow answer
// https://stackoverflow.com/a/14599265
// by user1129665

int main(int argc, char **argv)
{
    Display *display;
    XEvent xevent;
    Window window;

    if( (display = XOpenDisplay(NULL)) == NULL )
    {
        return -1;
    }

    window = DefaultRootWindow(display);
    XAllowEvents(display, AsyncBoth, CurrentTime);

    XGrabPointer(display,
                 window,
                 1,
                 PointerMotionMask | ButtonPressMask | ButtonReleaseMask,
                 GrabModeAsync,
                 GrabModeAsync,
                 None,
                 None,
                 CurrentTime);

    while(1) {
        XNextEvent(display, &xevent);

        switch (xevent.type) {
            case MotionNotify:
                printf(
                  "MV, %ld, %d, %d\n",
                  xevent.xmotion.time,
                  xevent.xmotion.x_root,
                  xevent.xmotion.y_root);
		        fflush(stdout); 
                break;
            case ButtonPress:
                printf(
                  "BTN_D, %ld, %d,\n",
                  xevent.xmotion.time,
                  xevent.xbutton.button);
		        fflush(stdout);
                break;
            case ButtonRelease:
                printf(
                  "BTN_U, %ld, %d,\n",
                  xevent.xmotion.time,
                  xevent.xbutton.button);
		        fflush(stdout);
                break;
        }
    }

    return 0;
}
