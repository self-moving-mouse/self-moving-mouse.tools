#ifndef HEADER_GUARD_TRACK174
#define HEADER_GUARD_TRACK174

#define TRACK174_TSTEP_MS 8
#define TRACK174_TOTAL_DELTA_X -47
#define TRACK174_TOTAL_DELTA_Y -109

const PROGMEM int8_t TRACK174_DXY[111][2] = {
    {1, -1},
    {2, 0},
    {2, 1},
    {3, 1},
    {4, 1},
    {3, 1},
    {4, 0},
    {5, 0},
    {4, 0},
    {5, 0},
    {4, 0},
    {5, 0},
    {3, 0},
    {3, 0},
    {2, 0},
    {1, 0},
    {2, 0},
    {1, -1},
    {1, 0},
    {0, 0},
    {0, -1},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {-1, 0},
    {-1, 0},
    {-2, 0},
    {-4, 1},
    {-6, 2},
    {-8, 3},
    {-9, 4},
    {-11, 5},
    {-9, 4},
    {-11, 7},
    {-12, 6},
    {-14, 7},
    {-14, 6},
    {-12, 5},
    {-11, 4},
    {-11, 4},
    {-9, 3},
    {-7, 1},
    {-4, 0},
    {-3, 0},
    {-2, 0},
    {-1, 0},
    {0, -2},
    {0, -4},
    {1, -5},
    {2, -7},
    {4, -7},
    {6, -9},
    {6, -7},
    {6, -6},
    {5, -4},
    {5, -2},
    {6, -1},
    {6, -2},
    {12, 3},
    {16, 6},
    {15, 9},
    {15, 11},
    {13, 12},
    {11, 8},
    {9, 7},
    {9, 6},
    {8, 3},
    {4, 1},
    {4, 1},
    {3, -1},
    {2, -3},
    {3, -5},
    {3, -6},
    {2, -8},
    {2, -11},
    {2, -15},
    {1, -14},
    {0, -15},
    {-1, -16},
    {-4, -14},
    {-5, -12},
    {-6, -9},
    {-10, -10},
    {-12, -9},
    {-17, -7},
    {-24, -6},
    {-26, -2},
    {-28, -2},
    {-26, 0},
    {-22, 2},
    {-14, 4},
    {-6, 2},
    {0, 0},
    {2, -1},
    {1, 0},
    {4, -2},
    {11, -4},
    {18, -6},
    {18, -6},
    {14, -3},
    {9, -3},
    {5, -4},
    {1, -3},
    {-1, -3},
    {-2, -3},
    {-2, 0},
    {-2, 1},
    {1, 4},
    {3, 7},
};

#define TRACK174_LENGTH (sizeof(TRACK174_DXY)/sizeof(TRACK174_DXY[0]))

#endif
