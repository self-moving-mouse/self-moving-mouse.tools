#ifndef HEADER_GUARD_TRACK181
#define HEADER_GUARD_TRACK181

#define TRACK181_TSTEP_MS 8
#define TRACK181_TOTAL_DELTA_X 138
#define TRACK181_TOTAL_DELTA_Y -91

const PROGMEM int8_t TRACK181_DXY[31][2] = {
    {2, -1},
    {3, -2},
    {6, -4},
    {11, -6},
    {14, -9},
    {15, -9},
    {19, -11},
    {16, -10},
    {13, -9},
    {8, -6},
    {11, -7},
    {8, -6},
    {5, -6},
    {5, -4},
    {4, -3},
    {2, -2},
    {1, -1},
    {1, -1},
    {0, -1},
    {0, 0},
    {0, 1},
    {-1, 0},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {0, 0},
    {0, 0},
    {0, 1},
    {0, 1},
    {-1, 0},
};

#define TRACK181_LENGTH (sizeof(TRACK181_DXY)/sizeof(TRACK181_DXY[0]))

#endif
