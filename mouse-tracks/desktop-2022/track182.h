#ifndef HEADER_GUARD_TRACK182
#define HEADER_GUARD_TRACK182

#define TRACK182_TSTEP_MS 8
#define TRACK182_TOTAL_DELTA_X -293
#define TRACK182_TOTAL_DELTA_Y 196

const PROGMEM int8_t TRACK182_DXY[17][2] = {
    {-1, 0},
    {-2, 1},
    {-4, 3},
    {-9, 8},
    {-14, 10},
    {-22, 14},
    {-24, 18},
    {-26, 18},
    {-26, 18},
    {-32, 22},
    {-28, 20},
    {-26, 16},
    {-24, 14},
    {-20, 12},
    {-15, 10},
    {-13, 7},
    {-7, 5},
};

#define TRACK182_LENGTH (sizeof(TRACK182_DXY)/sizeof(TRACK182_DXY[0]))

#endif
