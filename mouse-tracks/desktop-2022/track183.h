#ifndef HEADER_GUARD_TRACK183
#define HEADER_GUARD_TRACK183

#define TRACK183_TSTEP_MS 8
#define TRACK183_TOTAL_DELTA_X 261
#define TRACK183_TOTAL_DELTA_Y -62

const PROGMEM int8_t TRACK183_DXY[15][2] = {
    {1, 0},
    {4, -1},
    {8, -2},
    {13, -5},
    {18, -5},
    {22, -6},
    {30, -6},
    {28, -6},
    {28, -6},
    {28, -6},
    {26, -4},
    {22, -6},
    {18, -4},
    {10, -4},
    {5, -1},
};

#define TRACK183_LENGTH (sizeof(TRACK183_DXY)/sizeof(TRACK183_DXY[0]))

#endif
