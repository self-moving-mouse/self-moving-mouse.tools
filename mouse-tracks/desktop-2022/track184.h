#ifndef HEADER_GUARD_TRACK184
#define HEADER_GUARD_TRACK184

#define TRACK184_TSTEP_MS 8
#define TRACK184_TOTAL_DELTA_X -68
#define TRACK184_TOTAL_DELTA_Y 38

const PROGMEM int8_t TRACK184_DXY[26][2] = {
    {1, 0},
    {-2, 1},
    {-3, 1},
    {-5, 2},
    {-7, 4},
    {-4, 2},
    {-3, 3},
    {-3, 3},
    {-2, 2},
    {-2, 1},
    {-1, 1},
    {0, 0},
    {-1, 0},
    {-1, 1},
    {-1, 0},
    {-2, 1},
    {-2, 1},
    {-2, 1},
    {-3, 2},
    {-5, 2},
    {-6, 3},
    {-3, 2},
    {-4, 2},
    {-3, 1},
    {-2, 1},
    {-2, 1},
};

#define TRACK184_LENGTH (sizeof(TRACK184_DXY)/sizeof(TRACK184_DXY[0]))

#endif
