#ifndef HEADER_GUARD_TRACK185
#define HEADER_GUARD_TRACK185

#define TRACK185_TSTEP_MS 8
#define TRACK185_TOTAL_DELTA_X 189
#define TRACK185_TOTAL_DELTA_Y -118

const PROGMEM int8_t TRACK185_DXY[11][2] = {
    {2, -1},
    {4, -3},
    {11, -9},
    {25, -18},
    {26, -16},
    {26, -18},
    {28, -16},
    {26, -14},
    {19, -11},
    {14, -8},
    {8, -4},
};

#define TRACK185_LENGTH (sizeof(TRACK185_DXY)/sizeof(TRACK185_DXY[0]))

#endif
