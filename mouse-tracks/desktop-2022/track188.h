#ifndef HEADER_GUARD_TRACK188
#define HEADER_GUARD_TRACK188

#define TRACK188_TSTEP_MS 8
#define TRACK188_TOTAL_DELTA_X -14
#define TRACK188_TOTAL_DELTA_Y -8

const PROGMEM int8_t TRACK188_DXY[65][2] = {
    {4, 0},
    {9, -5},
    {15, -5},
    {20, -7},
    {27, -9},
    {24, -8},
    {19, -8},
    {9, -3},
    {4, -2},
    {2, 0},
    {-1, 0},
    {-1, 1},
    {-1, 0},
    {-1, 0},
    {-1, 0},
    {-4, 1},
    {-6, 2},
    {-8, 4},
    {-12, 5},
    {-13, 4},
    {-13, 3},
    {-9, 3},
    {-4, 3},
    {-3, 1},
    {0, 0},
    {0, 0},
    {1, 0},
    {6, -1},
    {15, -6},
    {24, -6},
    {24, -8},
    {19, -6},
    {9, -2},
    {1, -1},
    {-1, 0},
    {-2, 1},
    {-6, 3},
    {-13, 5},
    {-22, 5},
    {-27, 8},
    {-34, 10},
    {-32, 10},
    {-27, 6},
    {-21, 3},
    {-10, 2},
    {-2, 0},
    {-1, 0},
    {3, -1},
    {9, -3},
    {24, -6},
    {35, -8},
    {42, -12},
    {32, -8},
    {18, -5},
    {3, -1},
    {-3, 0},
    {-7, 2},
    {-10, 3},
    {-14, 3},
    {-16, 5},
    {-19, 6},
    {-21, 5},
    {-19, 3},
    {-17, 4},
    {-11, 2},
};

#define TRACK188_LENGTH (sizeof(TRACK188_DXY)/sizeof(TRACK188_DXY[0]))

#endif
