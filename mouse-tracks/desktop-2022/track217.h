#ifndef HEADER_GUARD_TRACK217
#define HEADER_GUARD_TRACK217

#define TRACK217_TSTEP_MS 8
#define TRACK217_TOTAL_DELTA_X 242
#define TRACK217_TOTAL_DELTA_Y 161

const PROGMEM int8_t TRACK217_DXY[144][2] = {
    {0, -1},
    {2, -1},
    {6, -2},
    {12, -5},
    {21, -8},
    {30, -12},
    {32, -14},
    {30, -18},
    {26, -18},
    {22, -18},
    {16, -16},
    {10, -13},
    {3, -14},
    {-1, -11},
    {-4, -15},
    {-7, -19},
    {-14, -22},
    {-24, -24},
    {-30, -24},
    {-42, -24},
    {-42, -18},
    {-40, -16},
    {-36, -12},
    {-30, -10},
    {-20, -8},
    {-9, -4},
    {-1, -2},
    {3, -2},
    {12, -6},
    {27, -11},
    {36, -16},
    {42, -16},
    {42, -16},
    {42, -16},
    {28, -14},
    {14, -8},
    {2, -4},
    {-3, -2},
    {-15, -5},
    {-30, -4},
    {-40, -2},
    {-44, 0},
    {-46, 0},
    {-46, 0},
    {-32, 0},
    {-20, 0},
    {-6, 0},
    {0, 0},
    {4, 0},
    {12, -1},
    {28, -4},
    {36, -6},
    {38, -10},
    {44, -12},
    {32, -8},
    {20, -8},
    {6, -3},
    {1, -1},
    {-5, -1},
    {-14, -2},
    {-27, -2},
    {-36, 0},
    {-38, 0},
    {-40, 2},
    {-36, 4},
    {-30, 6},
    {-16, 4},
    {-6, 1},
    {-1, 1},
    {4, 1},
    {12, 3},
    {25, 2},
    {32, 2},
    {38, 0},
    {42, 0},
    {42, 0},
    {42, 0},
    {30, 2},
    {18, 4},
    {8, 2},
    {1, 2},
    {-2, 4},
    {-9, 9},
    {-17, 13},
    {-24, 18},
    {-30, 20},
    {-32, 22},
    {-34, 26},
    {-24, 22},
    {-14, 18},
    {-2, 13},
    {6, 11},
    {19, 13},
    {32, 16},
    {52, 16},
    {60, 14},
    {62, 14},
    {58, 10},
    {52, 12},
    {38, 12},
    {24, 10},
    {10, 12},
    {-1, 7},
    {-13, 10},
    {-28, 16},
    {-40, 14},
    {-50, 14},
    {-64, 16},
    {-58, 14},
    {-50, 14},
    {-38, 14},
    {-24, 12},
    {-10, 9},
    {2, 5},
    {11, 6},
    {26, 8},
    {34, 8},
    {42, 8},
    {48, 8},
    {44, 6},
    {42, 6},
    {32, 6},
    {20, 8},
    {8, 3},
    {1, 4},
    {-4, 3},
    {-17, 9},
    {-28, 10},
    {-32, 12},
    {-38, 12},
    {-38, 12},
    {-34, 10},
    {-26, 12},
    {-15, 9},
    {-5, 7},
    {1, 3},
    {6, 4},
    {15, 4},
    {23, 6},
    {28, 6},
    {36, 6},
    {30, 4},
    {24, 2},
    {15, 2},
};

#define TRACK217_LENGTH (sizeof(TRACK217_DXY)/sizeof(TRACK217_DXY[0]))

#endif
