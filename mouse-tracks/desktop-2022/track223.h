#ifndef HEADER_GUARD_TRACK223
#define HEADER_GUARD_TRACK223

#define TRACK223_TSTEP_MS 8
#define TRACK223_TOTAL_DELTA_X 179
#define TRACK223_TOTAL_DELTA_Y -188

const PROGMEM int8_t TRACK223_DXY[127][2] = {
    {0, 0},
    {2, 0},
    {3, 0},
    {5, 1},
    {7, 0},
    {12, 1},
    {26, 2},
    {28, 0},
    {34, 0},
    {36, 0},
    {42, 0},
    {44, 0},
    {44, 0},
    {52, -4},
    {44, -4},
    {38, -6},
    {30, -8},
    {26, -10},
    {20, -12},
    {16, -14},
    {10, -18},
    {3, -17},
    {0, -19},
    {-8, -22},
    {-14, -26},
    {-20, -28},
    {-24, -28},
    {-34, -32},
    {-38, -30},
    {-40, -26},
    {-44, -20},
    {-48, -14},
    {-50, -8},
    {-50, -4},
    {-50, -2},
    {-50, 2},
    {-42, 6},
    {-34, 8},
    {-26, 12},
    {-16, 16},
    {-10, 18},
    {-2, 22},
    {8, 32},
    {16, 36},
    {24, 38},
    {32, 36},
    {38, 34},
    {40, 30},
    {44, 26},
    {50, 22},
    {46, 14},
    {42, 8},
    {36, 2},
    {28, -2},
    {24, -8},
    {18, -14},
    {10, -18},
    {2, -22},
    {-6, -26},
    {-12, -28},
    {-18, -32},
    {-24, -34},
    {-30, -34},
    {-36, -30},
    {-46, -26},
    {-44, -16},
    {-44, -8},
    {-42, -4},
    {-40, 0},
    {-36, 6},
    {-30, 12},
    {-24, 18},
    {-14, 24},
    {-8, 26},
    {0, 30},
    {6, 32},
    {14, 30},
    {18, 30},
    {28, 28},
    {32, 24},
    {36, 18},
    {38, 10},
    {42, 6},
    {42, 2},
    {42, -4},
    {44, -10},
    {34, -14},
    {28, -18},
    {18, -20},
    {10, -24},
    {2, -28},
    {-6, -30},
    {-16, -30},
    {-30, -26},
    {-34, -16},
    {-38, -10},
    {-38, -4},
    {-42, 2},
    {-42, 10},
    {-40, 16},
    {-38, 22},
    {-26, 24},
    {-16, 24},
    {-8, 24},
    {0, 20},
    {6, 17},
    {13, 11},
    {22, 6},
    {26, 0},
    {28, -8},
    {28, -14},
    {26, -16},
    {20, -18},
    {16, -20},
    {10, -22},
    {6, -17},
    {0, -15},
    {0, -7},
    {0, -3},
    {0, -1},
    {-1, 0},
    {0, 0},
    {-1, 0},
    {-1, 1},
    {-2, 1},
    {-2, 1},
    {-1, 0},
};

#define TRACK223_LENGTH (sizeof(TRACK223_DXY)/sizeof(TRACK223_DXY[0]))

#endif
