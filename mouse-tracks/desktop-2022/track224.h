#ifndef HEADER_GUARD_TRACK224
#define HEADER_GUARD_TRACK224

#define TRACK224_TSTEP_MS 8
#define TRACK224_TOTAL_DELTA_X 143
#define TRACK224_TOTAL_DELTA_Y 45

const PROGMEM int8_t TRACK224_DXY[133][2] = {
    {0, 3},
    {2, 6},
    {4, 8},
    {6, 9},
    {8, 10},
    {14, 10},
    {20, 12},
    {24, 10},
    {32, 8},
    {42, 6},
    {46, 4},
    {50, 0},
    {52, -2},
    {54, -6},
    {50, -8},
    {48, -12},
    {46, -16},
    {36, -18},
    {28, -20},
    {22, -22},
    {16, -26},
    {10, -24},
    {4, -26},
    {-6, -32},
    {-12, -30},
    {-20, -28},
    {-24, -24},
    {-30, -20},
    {-36, -16},
    {-40, -10},
    {-46, -6},
    {-60, -4},
    {-56, 2},
    {-58, 6},
    {-58, 10},
    {-54, 16},
    {-48, 22},
    {-40, 28},
    {-32, 36},
    {-20, 36},
    {-10, 36},
    {2, 34},
    {10, 30},
    {18, 26},
    {24, 22},
    {32, 16},
    {44, 12},
    {48, 8},
    {48, 4},
    {50, 2},
    {48, -4},
    {42, -8},
    {38, -10},
    {34, -18},
    {22, -18},
    {14, -22},
    {8, -24},
    {-4, -28},
    {-12, -28},
    {-20, -32},
    {-32, -34},
    {-38, -30},
    {-40, -20},
    {-42, -12},
    {-46, -6},
    {-46, 0},
    {-42, 8},
    {-38, 16},
    {-34, 24},
    {-22, 26},
    {-12, 28},
    {-6, 28},
    {4, 28},
    {12, 30},
    {18, 26},
    {22, 22},
    {24, 16},
    {30, 12},
    {38, 8},
    {38, 4},
    {36, 0},
    {36, -4},
    {30, -6},
    {26, -8},
    {20, -10},
    {16, -14},
    {8, -11},
    {2, -8},
    {-1, -6},
    {-5, -5},
    {-10, -2},
    {-17, -2},
    {-28, 2},
    {-44, 10},
    {-46, 14},
    {-48, 16},
    {-48, 18},
    {-44, 16},
    {-38, 16},
    {-28, 14},
    {-16, 10},
    {-6, 4},
    {0, 2},
    {4, 1},
    {13, -2},
    {26, -8},
    {34, -14},
    {42, -20},
    {42, -20},
    {38, -24},
    {36, -26},
    {30, -28},
    {22, -28},
    {16, -30},
    {6, -34},
    {-2, -26},
    {-8, -20},
    {-14, -12},
    {-16, -5},
    {-19, 1},
    {-24, 12},
    {-26, 22},
    {-22, 28},
    {-18, 30},
    {-12, 32},
    {-6, 30},
    {-4, 26},
    {2, 22},
    {3, 15},
    {3, 8},
    {3, 3},
    {2, 1},
    {-1, 1},
};

#define TRACK224_LENGTH (sizeof(TRACK224_DXY)/sizeof(TRACK224_DXY[0]))

#endif
