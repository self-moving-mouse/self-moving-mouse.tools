#ifndef HEADER_GUARD_TRACK131
#define HEADER_GUARD_TRACK131

#define TRACK131_TSTEP_MS 8
#define TRACK131_TOTAL_DELTA_X 58
#define TRACK131_TOTAL_DELTA_Y 46

const PROGMEM int8_t TRACK131_DXY[48][2] = {
    {0, 0},
    {1, 0},
    {2, 1},
    {2, 2},
    {2, 1},
    {2, 2},
    {3, 2},
    {3, 4},
    {7, 5},
    {5, 5},
    {5, 4},
    {5, 4},
    {4, 3},
    {4, 4},
    {4, 4},
    {5, 2},
    {2, 2},
    {2, 1},
    {1, 1},
    {1, 1},
    {1, 1},
    {0, 1},
    {1, 1},
    {0, 1},
    {0, 1},
    {1, 0},
    {0, 1},
    {0, 0},
    {0, 0},
    {-1, 1},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {-1, -1},
    {0, 0},
    {0, 0},
    {-1, -1},
    {0, 0},
    {0, -1},
    {-1, 0},
    {0, -1},
    {0, -1},
    {-1, 0},
    {0, -1},
    {0, -1},
    {0, -1},
    {0, -1},
};

#define TRACK131_LENGTH (sizeof(TRACK131_DXY)/sizeof(TRACK131_DXY[0]))

#endif
