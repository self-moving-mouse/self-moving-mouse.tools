#ifndef HEADER_GUARD_TRACK132
#define HEADER_GUARD_TRACK132

#define TRACK132_TSTEP_MS 8
#define TRACK132_TOTAL_DELTA_X -44
#define TRACK132_TOTAL_DELTA_Y 31

const PROGMEM int8_t TRACK132_DXY[34][2] = {
    {-1, 1},
    {-1, 0},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 0},
    {-2, 1},
    {-1, 1},
    {-2, 1},
    {-1, 1},
    {-2, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 0},
    {-2, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-2, 1},
    {-2, 1},
    {-2, 1},
    {-2, 1},
    {-2, 1},
    {-1, 1},
    {-2, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
    {-1, 1},
};

#define TRACK132_LENGTH (sizeof(TRACK132_DXY)/sizeof(TRACK132_DXY[0]))

#endif
