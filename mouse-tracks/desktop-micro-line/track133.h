#ifndef HEADER_GUARD_TRACK133
#define HEADER_GUARD_TRACK133

#define TRACK133_TSTEP_MS 8
#define TRACK133_TOTAL_DELTA_X -21
#define TRACK133_TOTAL_DELTA_Y -23

const PROGMEM int8_t TRACK133_DXY[30][2] = {
    {-1, -1},
    {-1, -1},
    {-1, -1},
    {-1, -1},
    {-1, -1},
    {-2, -2},
    {-1, -1},
    {-1, -1},
    {-2, -1},
    {-1, -2},
    {-1, -1},
    {-1, -1},
    {-1, -1},
    {-1, -1},
    {-1, -1},
    {-1, 0},
    {0, -1},
    {0, -1},
    {-1, 0},
    {-1, 0},
    {0, -1},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, -1},
    {0, 0},
    {0, 0},
    {-1, -1},
    {0, -1},
    {0, 0},
};

#define TRACK133_LENGTH (sizeof(TRACK133_DXY)/sizeof(TRACK133_DXY[0]))

#endif
