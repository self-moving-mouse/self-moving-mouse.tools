#ifndef HEADER_GUARD_TRACK134
#define HEADER_GUARD_TRACK134

#define TRACK134_TSTEP_MS 8
#define TRACK134_TOTAL_DELTA_X 57
#define TRACK134_TOTAL_DELTA_Y -25

const PROGMEM int8_t TRACK134_DXY[51][2] = {
    {1, 0},
    {1, 0},
    {1, 0},
    {0, -1},
    {1, 0},
    {2, -1},
    {2, -1},
    {2, -1},
    {2, -1},
    {3, -1},
    {3, -1},
    {3, -2},
    {3, -1},
    {3, -2},
    {3, -1},
    {3, -2},
    {3, -1},
    {3, -1},
    {2, -1},
    {3, -1},
    {2, -1},
    {2, -1},
    {3, -1},
    {2, 0},
    {2, -1},
    {1, 0},
    {1, -1},
    {1, 0},
    {0, 0},
    {1, -1},
    {1, 0},
    {1, 0},
    {1, 0},
    {1, 0},
    {0, 0},
    {1, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {-1, 0},
    {0, 0},
    {-1, 0},
    {0, 0},
    {-1, 0},
    {-1, 0},
    {-1, 0},
    {-1, 0},
    {-1, 0},
};

#define TRACK134_LENGTH (sizeof(TRACK134_DXY)/sizeof(TRACK134_DXY[0]))

#endif
