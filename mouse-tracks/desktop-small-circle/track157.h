#ifndef HEADER_GUARD_TRACK157
#define HEADER_GUARD_TRACK157

#define TRACK157_TSTEP_MS 8
#define TRACK157_TOTAL_DELTA_X -122
#define TRACK157_TOTAL_DELTA_Y -63

const PROGMEM int8_t TRACK157_DXY[163][2] = {
    {1, 0},
    {2, 0},
    {3, 0},
    {4, 1},
    {5, 2},
    {6, 4},
    {7, 5},
    {6, 8},
    {6, 10},
    {5, 14},
    {2, 14},
    {0, 13},
    {-4, 13},
    {-5, 12},
    {-6, 9},
    {-7, 6},
    {-11, 5},
    {-13, 2},
    {-16, 0},
    {-21, -4},
    {-22, -6},
    {-22, -10},
    {-20, -12},
    {-18, -16},
    {-10, -16},
    {-6, -17},
    {4, -19},
    {10, -20},
    {16, -20},
    {22, -16},
    {26, -10},
    {24, -6},
    {20, -2},
    {12, 2},
    {7, 4},
    {4, 6},
    {2, 7},
    {1, 13},
    {-5, 18},
    {-12, 20},
    {-18, 20},
    {-22, 18},
    {-26, 16},
    {-28, 14},
    {-26, 10},
    {-28, 6},
    {-24, 2},
    {-18, -4},
    {-10, -7},
    {-3, -7},
    {0, -12},
    {6, -18},
    {11, -28},
    {20, -28},
    {24, -26},
    {30, -22},
    {34, -16},
    {36, -10},
    {34, -6},
    {34, 2},
    {28, 6},
    {20, 8},
    {12, 11},
    {5, 12},
    {0, 15},
    {-10, 20},
    {-16, 20},
    {-28, 22},
    {-32, 18},
    {-34, 12},
    {-34, 8},
    {-34, 2},
    {-32, -4},
    {-28, -8},
    {-22, -16},
    {-14, -18},
    {-8, -20},
    {2, -24},
    {8, -22},
    {16, -20},
    {22, -18},
    {30, -12},
    {34, -6},
    {32, -2},
    {32, 6},
    {30, 10},
    {24, 14},
    {20, 16},
    {14, 20},
    {6, 20},
    {0, 17},
    {-7, 14},
    {-13, 13},
    {-20, 12},
    {-26, 8},
    {-30, 4},
    {-40, -4},
    {-38, -10},
    {-34, -14},
    {-28, -14},
    {-22, -18},
    {-12, -18},
    {-6, -20},
    {6, -20},
    {14, -18},
    {22, -14},
    {30, -10},
    {34, -6},
    {34, -2},
    {30, 4},
    {26, 10},
    {16, 12},
    {9, 13},
    {2, 11},
    {-5, 13},
    {-11, 17},
    {-18, 18},
    {-24, 16},
    {-30, 14},
    {-32, 8},
    {-32, 4},
    {-30, 0},
    {-26, -6},
    {-22, -8},
    {-14, -9},
    {-5, -14},
    {2, -16},
    {11, -20},
    {18, -18},
    {26, -18},
    {30, -14},
    {34, -8},
    {40, -4},
    {34, 2},
    {30, 8},
    {20, 12},
    {14, 14},
    {6, 13},
    {0, 13},
    {-5, 13},
    {-18, 16},
    {-24, 12},
    {-28, 8},
    {-32, 4},
    {-30, 0},
    {-28, -6},
    {-20, -6},
    {-11, -7},
    {-4, -8},
    {0, -6},
    {6, -9},
    {9, -8},
    {10, -8},
    {12, -6},
    {9, -3},
    {4, -3},
    {2, -2},
    {0, 0},
    {0, 0},
    {-1, 1},
    {0, 0},
    {-1, 0},
    {-1, 0},
};

#define TRACK157_LENGTH (sizeof(TRACK157_DXY)/sizeof(TRACK157_DXY[0]))

#endif
