#ifndef HEADER_GUARD_TRACK145
#define HEADER_GUARD_TRACK145

#define TRACK145_TSTEP_MS 8
#define TRACK145_TOTAL_DELTA_X -18
#define TRACK145_TOTAL_DELTA_Y 68

const PROGMEM int8_t TRACK145_DXY[161][2] = {
    {-1, -1},
    {0, -1},
    {-1, -1},
    {0, -1},
    {0, -1},
    {0, -2},
    {-1, -3},
    {-1, -3},
    {-1, -3},
    {-1, -4},
    {-1, -3},
    {-1, -3},
    {-1, -3},
    {-1, -3},
    {0, -2},
    {0, -2},
    {0, -2},
    {0, -2},
    {0, 0},
    {1, -1},
    {0, 1},
    {0, 1},
    {-1, 2},
    {-1, 3},
    {-1, 5},
    {-1, 7},
    {-2, 8},
    {-1, 8},
    {-1, 10},
    {-3, 11},
    {-4, 11},
    {-3, 9},
    {-1, 8},
    {-1, 7},
    {-2, 6},
    {-1, 4},
    {0, 2},
    {0, 1},
    {0, 0},
    {1, 0},
    {0, -1},
    {1, -3},
    {2, -5},
    {5, -11},
    {7, -13},
    {7, -17},
    {7, -15},
    {8, -14},
    {6, -12},
    {5, -8},
    {2, -3},
    {1, -1},
    {0, 0},
    {0, 1},
    {-1, 3},
    {-2, 7},
    {-5, 12},
    {-7, 14},
    {-10, 22},
    {-10, 20},
    {-9, 18},
    {-8, 15},
    {-7, 10},
    {-2, 5},
    {-1, 3},
    {0, 1},
    {0, 0},
    {1, 0},
    {1, -2},
    {3, -5},
    {4, -8},
    {6, -11},
    {5, -11},
    {5, -12},
    {3, -9},
    {2, -5},
    {1, -2},
    {0, 0},
    {0, 0},
    {0, 1},
    {-1, 5},
    {-3, 12},
    {-3, 14},
    {-3, 11},
    {-3, 11},
    {-3, 9},
    {-3, 7},
    {-1, 5},
    {0, 2},
    {0, 1},
    {-1, -1},
    {0, -1},
    {0, -1},
    {2, -4},
    {1, -7},
    {3, -8},
    {1, -8},
    {2, -8},
    {1, -9},
    {1, -7},
    {1, -2},
    {0, -1},
    {0, 0},
    {0, 0},
    {0, 2},
    {0, 6},
    {0, 6},
    {0, 10},
    {0, 8},
    {0, 7},
    {-1, 3},
    {-1, 1},
    {1, 0},
    {0, 0},
    {0, -1},
    {1, -3},
    {1, -7},
    {2, -12},
    {2, -13},
    {3, -16},
    {3, -12},
    {3, -9},
    {1, -4},
    {1, 1},
    {0, 1},
    {1, 1},
    {0, 4},
    {0, 8},
    {0, 12},
    {-4, 13},
    {-3, 12},
    {-3, 13},
    {-3, 9},
    {-3, 7},
    {-3, 3},
    {-1, 1},
    {0, 0},
    {0, 0},
    {0, -1},
    {1, -3},
    {2, -5},
    {1, -7},
    {1, -4},
    {1, -2},
    {0, -1},
    {0, 0},
    {0, 0},
    {0, 1},
    {0, 1},
    {-1, 2},
    {0, 1},
    {0, 1},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, -1},
    {0, -2},
    {0, -3},
    {0, -3},
    {0, -2},
    {0, -2},
};

#define TRACK145_LENGTH (sizeof(TRACK145_DXY)/sizeof(TRACK145_DXY[0]))

#endif
