#ifndef HEADER_GUARD_TRACK101
#define HEADER_GUARD_TRACK101

#define TRACK101_TSTEP_MS 8
#define TRACK101_TOTAL_DELTA_X 40
#define TRACK101_TOTAL_DELTA_Y 0

const PROGMEM int8_t TRACK101_DXY[4][2] = {
    {10, 5},
    {10, 0},
    {10, 0},
    {10, -5},
};

#define TRACK101_LENGTH (sizeof(TRACK101_DXY)/sizeof(TRACK101_DXY[0]))

#endif
