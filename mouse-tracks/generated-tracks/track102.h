#ifndef HEADER_GUARD_TRACK102
#define HEADER_GUARD_TRACK102

#define TRACK102_TSTEP_MS 8
#define TRACK102_TOTAL_DELTA_X -40
#define TRACK102_TOTAL_DELTA_Y 0

const PROGMEM int8_t TRACK102_DXY[4][2] = {
    {-10, -5},
    {-10, 0},
    {-10, 0},
    {-10, 5},
};

#define TRACK102_LENGTH (sizeof(TRACK102_DXY)/sizeof(TRACK102_DXY[0]))

#endif
