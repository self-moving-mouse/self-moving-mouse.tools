#ifndef HEADER_GUARD_TRACK103
#define HEADER_GUARD_TRACK103

#define TRACK103_TSTEP_MS 8
#define TRACK103_TOTAL_DELTA_X 0
#define TRACK103_TOTAL_DELTA_Y 40

const PROGMEM int8_t TRACK103_DXY[4][2] = {
    {5, 10},
    {0, 10},
    {0, 10},
    {-5, 10},
};

#define TRACK103_LENGTH (sizeof(TRACK103_DXY)/sizeof(TRACK103_DXY[0]))

#endif
