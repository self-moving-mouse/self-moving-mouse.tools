#ifndef HEADER_GUARD_TRACK104
#define HEADER_GUARD_TRACK104

#define TRACK104_TSTEP_MS 8
#define TRACK104_TOTAL_DELTA_X 0
#define TRACK104_TOTAL_DELTA_Y -40

const PROGMEM int8_t TRACK104_DXY[4][2] = {
    {-5, -10},
    {0, -10},
    {0, -10},
    {5, -10},
};

#define TRACK104_LENGTH (sizeof(TRACK104_DXY)/sizeof(TRACK104_DXY[0]))

#endif
