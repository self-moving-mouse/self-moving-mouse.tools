#ifndef HEADER_GUARD_TRACK001
#define HEADER_GUARD_TRACK001

#define TRACK001_TSTEP_MS 8
#define TRACK001_TOTAL_DELTA_X 48
#define TRACK001_TOTAL_DELTA_Y -171

const PROGMEM int8_t TRACK001_DXY[435][2] = {
    {1, -1},
    {10, -1},
    {25, -6},
    {40, -7},
    {51, -8},
    {57, -6},
    {64, -7},
    {48, -4},
    {30, -4},
    {13, -2},
    {4, 0},
    {1, 0},
    {-6, 0},
    {-29, 0},
    {-62, 0},
    {-80, 0},
    {-118, 0},
    {-118, 6},
    {-122, 10},
    {-110, 8},
    {-92, 10},
    {-66, 8},
    {-36, 6},
    {-15, 4},
    {1, 2},
    {10, 2},
    {29, 2},
    {50, 2},
    {58, 2},
    {62, 0},
    {60, 0},
    {54, 0},
    {42, 0},
    {26, 0},
    {11, 0},
    {2, 0},
    {-9, -1},
    {-23, 0},
    {-42, 0},
    {-62, 0},
    {-64, 0},
    {-58, 0},
    {-50, 2},
    {-30, 2},
    {-14, 0},
    {-2, 0},
    {5, 0},
    {18, 0},
    {34, 0},
    {52, 0},
    {56, 2},
    {58, 0},
    {54, 2},
    {46, 2},
    {34, 4},
    {13, 1},
    {2, 1},
    {1, 2},
    {-7, 2},
    {-21, 5},
    {-38, 6},
    {-48, 4},
    {-54, 2},
    {-44, 0},
    {-28, 0},
    {-14, 0},
    {-4, 0},
    {5, 0},
    {11, -2},
    {23, -6},
    {34, -6},
    {38, -6},
    {37, -4},
    {41, -4},
    {30, -2},
    {20, 0},
    {8, 0},
    {2, 0},
    {0, 0},
    {0, 0},
    {1, 0},
    {-2, 0},
    {-4, 0},
    {-6, 0},
    {-6, 0},
    {-4, 0},
    {-3, 0},
    {-3, 0},
    {-2, 0},
    {-2, 0},
    {-1, 0},
    {-1, 0},
    {-1, 0},
    {0, -1},
    {0, 0},
    {0, 0},
    {1, 0},
    {0, 0},
    {1, 0},
    {1, 0},
    {1, 0},
    {2, 0},
    {1, 0},
    {2, 1},
    {1, 0},
    {2, 0},
    {2, 0},
    {4, -1},
    {8, -1},
    {16, -2},
    {20, -2},
    {19, 0},
    {15, 0},
    {9, 0},
    {2, 0},
    {0, 0},
    {1, 0},
    {-7, 2},
    {-19, 5},
    {-40, 8},
    {-56, 10},
    {-72, 10},
    {-84, 8},
    {-108, 4},
    {-102, 2},
    {-100, 0},
    {-88, -2},
    {-74, 0},
    {-48, 0},
    {-26, 0},
    {-7, -1},
    {12, -5},
    {37, -5},
    {60, -6},
    {98, -6},
    {108, -4},
    {122, -2},
    {127, -2},
    {126, 0},
    {116, -2},
    {98, -2},
    {76, 0},
    {40, 2},
    {13, 2},
    {-8, 0},
    {-28, 1},
    {-75, 0},
    {-80, 0},
    {-98, 0},
    {-110, -2},
    {-116, -4},
    {-112, -8},
    {-100, -6},
    {-76, -4},
    {-38, -2},
    {-9, -3},
    {16, -2},
    {46, -2},
    {86, 2},
    {102, 2},
    {118, 4},
    {124, 6},
    {126, 4},
    {112, 4},
    {90, 4},
    {62, 4},
    {30, 2},
    {7, 0},
    {-11, -1},
    {-29, -4},
    {-48, -6},
    {-62, -8},
    {-72, -12},
    {-80, -14},
    {-90, -16},
    {-76, -14},
    {-56, -10},
    {-32, -6},
    {-14, -4},
    {-2, -1},
    {4, -2},
    {11, -2},
    {23, -2},
    {28, 0},
    {28, 0},
    {24, 0},
    {15, 0},
    {11, 0},
    {9, 0},
    {7, 0},
    {4, 0},
    {4, 0},
    {1, 0},
    {1, 0},
    {-1, 0},
    {-2, 0},
    {-2, 0},
    {-3, 0},
    {-4, 0},
    {-4, 0},
    {-4, 0},
    {-3, 0},
    {-4, 0},
    {-3, 0},
    {-3, 0},
    {-1, 0},
    {-1, 0},
    {1, 0},
    {2, 0},
    {3, 0},
    {2, 0},
    {2, -1},
    {3, -1},
    {4, -1},
    {3, -1},
    {2, -1},
    {1, -1},
    {1, 0},
    {1, 0},
    {-2, 1},
    {-4, 1},
    {-14, 3},
    {-29, 4},
    {-41, 2},
    {-66, 2},
    {-77, 0},
    {-85, -2},
    {-95, -5},
    {-98, -6},
    {-98, -8},
    {-91, -8},
    {-77, -8},
    {-48, -5},
    {-23, -4},
    {1, -3},
    {21, -7},
    {69, -6},
    {92, -6},
    {120, -5},
    {127, -3},
    {127, -5},
    {127, -2},
    {127, -2},
    {127, 0},
    {101, 3},
    {59, 4},
    {24, 5},
    {-9, 3},
    {-42, 4},
    {-76, 2},
    {-128, 2},
    {-128, -2},
    {-128, -3},
    {-128, -6},
    {-128, -6},
    {-128, -4},
    {-128, -6},
    {-95, -5},
    {-50, -3},
    {-10, -5},
    {22, -6},
    {69, -5},
    {94, -2},
    {114, -2},
    {125, 0},
    {127, 0},
    {120, 2},
    {101, 5},
    {79, 8},
    {44, 5},
    {21, 4},
    {-1, 3},
    {-10, 3},
    {-32, 4},
    {-46, 2},
    {-59, 0},
    {-67, 0},
    {-76, 0},
    {-68, 0},
    {-53, 0},
    {-40, 0},
    {-24, 0},
    {-13, 0},
    {-7, 0},
    {-1, 0},
    {2, 0},
    {4, 0},
    {3, 0},
    {3, 0},
    {0, 0},
    {-2, 0},
    {0, 0},
    {1, 0},
    {2, -1},
    {6, -1},
    {12, 0},
    {26, 0},
    {38, 0},
    {53, 0},
    {56, 2},
    {58, 3},
    {56, 5},
    {54, 3},
    {53, 6},
    {40, 5},
    {27, 4},
    {17, 2},
    {9, 1},
    {3, 0},
    {-1, 0},
    {-2, 0},
    {-2, 0},
    {1, 1},
    {1, 1},
    {1, 0},
    {1, 1},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, -1},
    {-1, 0},
    {0, 0},
    {0, -1},
    {-1, 0},
    {0, 0},
    {0, 0},
    {1, 0},
    {2, -1},
    {5, -1},
    {7, -2},
    {14, -5},
    {19, -4},
    {20, -5},
    {18, -5},
    {16, -2},
    {13, -2},
    {7, 0},
    {5, 0},
    {2, 0},
    {0, 0},
    {-1, -1},
    {-2, 0},
    {-2, 1},
    {-2, 1},
    {-3, 1},
    {-8, 3},
    {-11, 3},
    {-14, 3},
    {-13, 2},
    {-11, 1},
    {-4, 2},
    {0, 0},
    {-1, 0},
    {-1, 0},
    {5, -1},
    {19, -4},
    {34, -2},
    {47, 0},
    {53, 0},
    {56, 0},
    {56, 2},
    {52, 2},
    {34, 4},
    {20, 2},
    {6, 2},
    {-3, 2},
    {-20, 6},
    {-41, 10},
    {-60, 12},
    {-75, 10},
    {-89, 8},
    {-92, 5},
    {-108, 2},
    {-95, 2},
    {-74, 2},
    {-48, 0},
    {-23, 0},
    {0, 0},
    {27, 0},
    {56, 0},
    {85, 2},
    {107, 2},
    {127, 0},
    {127, 0},
    {127, 0},
    {119, 3},
    {30, 6},
    {0, 5},
    {0, 3},
    {-2, 5},
    {-29, 9},
    {-55, 8},
    {-79, 10},
    {-102, 6},
    {-120, 5},
    {-128, 2},
    {-128, 0},
    {-122, 0},
    {-90, 0},
    {-51, 0},
    {-15, 0},
    {16, 2},
    {66, 2},
    {97, 0},
    {127, 0},
    {127, 0},
    {127, -2},
    {127, -2},
    {127, -2},
    {33, 2},
    {-1, 2},
    {0, 2},
    {0, 0},
    {-7, 1},
    {-35, 2},
    {-59, 0},
    {-83, -2},
    {-104, -1},
    {-128, -8},
    {-128, -8},
    {-128, -6},
    {-109, -5},
    {-77, -4},
    {-42, -2},
    {-11, -2},
    {14, -2},
    {45, -2},
    {67, -2},
    {88, -7},
    {99, -10},
    {113, -8},
    {99, -15},
    {75, 29},
    {48, -128},
};

#define TRACK001_LENGTH (sizeof(TRACK001_DXY)/sizeof(TRACK001_DXY[0]))

#endif
