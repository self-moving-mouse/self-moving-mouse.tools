#ifndef HEADER_GUARD_TRACK088
#define HEADER_GUARD_TRACK088

#define TRACK088_TSTEP_MS 8
#define TRACK088_TOTAL_DELTA_X -88
#define TRACK088_TOTAL_DELTA_Y -68

const PROGMEM int8_t TRACK088_DXY[119][2] = {
    {-3, -7},
    {-1, -8},
    {2, -9},
    {6, -10},
    {10, -9},
    {13, -8},
    {15, -6},
    {16, -4},
    {15, -2},
    {12, 0},
    {11, 2},
    {11, 2},
    {8, 4},
    {4, 4},
    {4, 6},
    {2, 7},
    {-1, 10},
    {-4, 11},
    {-7, 10},
    {-10, 9},
    {-16, 14},
    {-22, 16},
    {-22, 12},
    {-19, 10},
    {-15, 7},
    {-9, 3},
    {-4, 1},
    {-1, -1},
    {2, -2},
    {10, -7},
    {27, -17},
    {56, -31},
    {79, -38},
    {84, -32},
    {63, -18},
    {34, -3},
    {12, 2},
    {6, 4},
    {3, 4},
    {-3, 9},
    {-7, 12},
    {-15, 14},
    {-28, 22},
    {-37, 28},
    {-40, 30},
    {-33, 25},
    {-21, 16},
    {-9, 7},
    {-4, 2},
    {-1, 2},
    {2, 1},
    {9, -1},
    {21, -3},
    {32, -3},
    {32, -2},
    {24, 2},
    {13, 3},
    {6, 4},
    {0, 5},
    {-4, 8},
    {-16, 14},
    {-39, 24},
    {-80, 32},
    {-104, 31},
    {-99, 21},
    {-90, 8},
    {-74, -4},
    {-50, -14},
    {-29, -17},
    {-13, -18},
    {-7, -22},
    {1, -29},
    {12, -34},
    {18, -31},
    {26, -29},
    {33, -27},
    {33, -20},
    {26, -12},
    {20, -7},
    {14, -3},
    {6, -1},
    {3, 0},
    {1, 0},
    {-1, 2},
    {-8, 6},
    {-19, 11},
    {-29, 14},
    {-20, 10},
    {-13, 5},
    {-12, 4},
    {-19, 8},
    {-18, 6},
    {-8, 2},
    {-3, 0},
    {-1, 0},
    {2, -3},
    {15, -10},
    {31, -18},
    {44, -25},
    {46, -25},
    {34, -17},
    {15, -7},
    {6, -3},
    {3, -1},
    {0, 0},
    {-5, 2},
    {-13, 4},
    {-21, 8},
    {-25, 9},
    {-22, 7},
    {-12, 3},
    {-5, 1},
    {-3, 0},
    {-1, 0},
    {1, -2},
    {6, -3},
    {12, -5},
    {21, -8},
    {34, -12},
};

#define TRACK088_LENGTH (sizeof(TRACK088_DXY)/sizeof(TRACK088_DXY[0]))

#endif
