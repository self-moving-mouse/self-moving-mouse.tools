#!/usr/bin/env bash

## this script uses picocom (https://github.com/npat-efault/picocom)
## to save raw bytes from serial port 
## to file specified in first command line argument

SERIAL_PORT=/dev/ttyACM0

picocom $SERIAL_PORT \
    -b 9600 \
    -n -r --imap 'spchex,tabhex,crhex,lfhex,8bithex,nrmhex' \
    -g "$1"