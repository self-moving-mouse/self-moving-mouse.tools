#include <DigiCDC.h>

void setup() {  
  SerialUSB.begin();   
  SerialUSB.delay(500); 
}

void loop() {  
  
  // sample is 8 bit. 
  // higher bits of millis() are ignored
  uint8_t randomSample = millis();
  SerialUSB.refresh();
  SerialUSB.write(randomSample);   
  SerialUSB.delay(50); 
  
}
