#include <DigiCDC.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "microrng.h"

static RNGState rngState;

void setup() {  
  setupWatchdogTimer();
  SerialUSB.begin(); 
  //do {} while(!updateRandomSample());
}

void loop() {  
  if(updateRandomSample(&rngState))
  {
    uint8_t randomSample = useRandomNumber(&rngState);
    SerialUSB.refresh();
    SerialUSB.write(randomSample);   
    SerialUSB.delay(50); 
  }
  
}

inline void setupWatchdogTimer() {
  cli();
  MCUSR = 0;
  
  // Start timed sequence
  WDTCR |= _BV(WDCE) | _BV(WDE);

  // Put WDT into interrupt mode 
  // Set shortest prescaler(time-out) value = 2048 cycles (~16 ms) 
  WDTCR = _BV(WDIE);

  sei();
}

ISR(WDT_vect)
{
  RNGOnWatchdog(&rngState);
}
