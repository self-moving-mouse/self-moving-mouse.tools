#!/usr/bin/env bash

INPUT_DIR=raw-data
OUTPUT_DIR=test-results
SP800_22="python sp800_22_tests/sp800_22_tests.py"


$SP800_22 $INPUT_DIR/randomsamples202001161458.bin \
    > $OUTPUT_DIR/sp800_22.randomsamples202001161458.txt
$SP800_22 $INPUT_DIR/millis202001161818.bin \
    > $OUTPUT_DIR/sp800_22.millis202001161818.txt
$SP800_22 $INPUT_DIR/numpyrandint202101311955.bin \
    > $OUTPUT_DIR/sp800_22.numpyrandint202101311955.txt
