Tests of Distinguishability from Random
TEST: monobit_test
  Ones count   = 269599
  Zeroes count = 267353
  FAIL
  P=0.0021761072543424642
TEST: frequency_within_block_test
  n = 536952
  N = 99
  M = 5423
  PASS
  P=1.0001420677999866
TEST: runs_test
  prop  0.5020914346161296
  tau  0.002729369056175252
  vobs  268621.0
  PASS
  P=0.6828431418026002
TEST: longest_run_ones_in_a_block_test
  n = 536952
  K = 5
  M = 128
  N = 49
  chi_sq = 1.1220380543905262
  PASS
  P=0.9521344980271444
TEST: binary_matrix_rank_test
  Number of blocks 524
  Data bits used: 536576
  Data bits discarded: 376
  Full Rank Count  =  40
  Full Rank -1 Count =  185
  Remainder Count =  299
  Chi-Square =  876.3568123826267
  FAIL
  P=5.0296307433363136e-191
TEST: dft_test
  N0 = 255052.200000
  N1 = 249880.000000
  FAIL
  P=0.0
TEST: non_overlapping_template_matching_test
  PASS
  P=1.0000234717362915
TEST: overlapping_template_matching_test
Insufficient data. 536952 bit provided. 1,028,016 bits required
  FAIL
  P=0.0
TEST: maurers_universal_test
  sum = 466461.35366962035
  fn = 5.24986892438685
  FAIL
  P=1.0424960518199784e-22
TEST: linear_complexity_test
Error. Need at least 10^6 bits
  FAIL
  P=0.0
TEST: serial_test
  psi_sq_m   =  44.838093535392545
  psi_sq_mm1 =  29.609231365146115
  psi_sq_mm2 =  18.943920499412343
  delta1     =  15.22886217024643
  delta2     =  4.563551304512657
  P1         =  0.05484519802495904
  P2         =  0.3350783081017232
  PASS
P=0.05484519802495904
P=0.3350783081017232
TEST: approximate_entropy_test
  n         =  536952
  m         =  3
  Pattern 1 of 8, count = 66333
  Pattern 2 of 8, count = 66710
  Pattern 3 of 8, count = 66958
  Pattern 4 of 8, count = 67352
  Pattern 5 of 8, count = 66710
  Pattern 6 of 8, count = 67600
  Pattern 7 of 8, count = 67352
  Pattern 8 of 8, count = 67937
  phi(3)    = -4.381999
  Pattern 1 of 16, count = 32948
  Pattern 2 of 16, count = 33385
  Pattern 3 of 16, count = 33149
  Pattern 4 of 16, count = 33561
  Pattern 5 of 16, count = 33297
  Pattern 6 of 16, count = 33661
  Pattern 7 of 16, count = 33628
  Pattern 8 of 16, count = 33724
  Pattern 9 of 16, count = 33385
  Pattern 10 of 16, count = 33325
  Pattern 11 of 16, count = 33809
  Pattern 12 of 16, count = 33791
  Pattern 13 of 16, count = 33413
  Pattern 14 of 16, count = 33939
  Pattern 15 of 16, count = 33724
  Pattern 16 of 16, count = 34213
  phi(3)    = -5.075132
  AppEn(3)  = 0.693133
  ChiSquare =  15.225609904949097
  PASS
  P=0.054904248812449735
TEST: cumulative_sums_test
FAIL: Data not random
  FAIL
P=0.004100950450562246
P=0.003361652738806953
TEST: random_excursion_test
J=483
x = -4	chisq = 1.921958	p = 0.859835 
x = -3	chisq = 2.476005	p = 0.780104 
x = -2	chisq = 1.539735	p = 0.908439 
x = -1	chisq = 9.593840	p = 0.087596 
x = 1	chisq = 3.719232	p = 0.590505 
x = 2	chisq = 2.544285	p = 0.769811 
x = 3	chisq = 6.643021	p = 0.248571 
x = 4	chisq = 7.917018	p = 0.160869 
J too small (J < 500) for result to be reliable
  PASS
P=0.8598348215701637
P=0.7801042940516153
P=0.9084393741974374
P=0.08759637286289831
P=0.5905045756424089
P=0.7698114489478483
P=0.24857128369174816
P=0.1608686315636955
TEST: random_excursion_variant_test
J= 483
x = -9	 count=342	p = 0.271207 
x = -8	 count=356	p = 0.291406 
x = -7	 count=390	p = 0.406599 
x = -6	 count=445	p = 0.712398 
x = -5	 count=484	p = 0.991443 
x = -4	 count=521	p = 0.644002 
x = -3	 count=543	p = 0.387955 
x = -2	 count=527	p = 0.413733 
x = -1	 count=509	p = 0.402853 
x = 1	 count=464	p = 0.540991 
x = 2	 count=476	p = 0.896541 
x = 3	 count=513	p = 0.665984 
x = 4	 count=527	p = 0.592598 
x = 5	 count=504	p = 0.821807 
x = 6	 count=471	p = 0.907326 
x = 7	 count=457	p = 0.816528 
x = 8	 count=458	p = 0.835475 
x = 9	 count=472	p = 0.931595 
J too small (J=483 < 500) for result to be reliable
  PASS
P=0.27120697640451513
P=0.2914063614200767
P=0.40659862624952947
P=0.7123984598965775
P=0.9914429923931223
P=0.6440019459096264
P=0.38795512282614564
P=0.4137333662634764
P=0.40285327120539083
P=0.5409910760687735
P=0.896541439245506
P=0.6659842930165438
P=0.5925976274543794
P=0.8218071136764855
P=0.9073263250317443
P=0.8165275893840995
P=0.8354746747548649
P=0.9315951871376362

SUMMARY
-------
monobit_test, FALSE, 0.002
frequency_within_block_test, TRUE, 1.000
runs_test, TRUE, 0.683
longest_run_ones_in_a_block_test, TRUE, 0.952
binary_matrix_rank_test, FALSE, 0.000
dft_test, FALSE, 0.000
non_overlapping_template_matching_test, TRUE, 1.000
overlapping_template_matching_test, FALSE, 0.000
maurers_universal_test, FALSE, 0.000
linear_complexity_test, FALSE, 0.000
serial_test, TRUE, 0.055
approximate_entropy_test, TRUE, 0.055
cumulative_sums_test, FALSE, 0.003
random_excursion_test, TRUE, 0.088
random_excursion_variant_test, TRUE, 0.271
