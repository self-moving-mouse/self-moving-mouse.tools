#!/usr/bin/env fish

for f in (ls ddpcmv2_thread*.hdf5)
  for g in (h5ls $f | awk '{print $1}')
    echo h5copy -i $f -o ddpcmv3.hdf5 -s $g -d $g
    h5copy -i $f -o ddpcmv3.hdf5 -s $g -d $g
  end
end
