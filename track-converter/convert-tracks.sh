#!/usr/bin/env bash


function convert_track() {
    echo "converting $1"
    local track_folder_path="$(dirname $1)"
    local track_path_no_ext="$(basename -- $1)"
    local track_name="${track_path_no_ext%.*}"  
    #echo $track_folder_path
    #echo $track_path_no_ext
    #echo $track_name
    
    ./mouse_track_converter.py \
        --input-file "$1" \
        --output-header "${track_folder_path}/${track_name}.h" \
        --output-plot "${track_folder_path}/${track_name}.png" \
        --arduino \
        --force-tstep 8 \
        --track-name "${track_name^^}" \
        --output-csv "${track_folder_path}/interpolated_${track_name}.csv"                
}

function convert_all() {
    for f in $(ls $1/track*.csv); do
        convert_track "$f"
    done
}

convert_all "../mouse-tracks/misc"
convert_all "../mouse-tracks/generated-tracks"
convert_all "../mouse-tracks/desktop-small-line"
convert_all "../mouse-tracks/desktop-small-circle"
convert_all "../mouse-tracks/desktop-micro-line"
convert_all "../mouse-tracks/desktop-2022"
