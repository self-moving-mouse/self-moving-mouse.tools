#!/usr/bin/env python

import getopt
import sys
import os
import re

import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import chevron


import matplotlib.pyplot as plt

import mouse_track_converter as mtc

from trackencoders import \
    DPCM, \
    DPCM_Multidimentional_FullTable, \
    DDPCM, \
    DDPCM_Multidimentional_FullTable, \
    DPCMDiffTableOptimizer, \
    DDPCMDiffTableOptimizer, \
    DiffTableOptimizerHistory, \
    get_error_multitrack_maxnorm, \
    get_error_singletrack_maxnorm, \
    write_diff_table_c_data_header, \
    write_ddpcm_encoded_track_c_data_header, \
    write_dpcm_encoded_track_c_data_header, \
    write_tracks_library_c_data_header, \
    reflect_differences, \
    DIFFERENCES_8POWERS_OF_2, \
    DIFFERENCES_16, \
    ErrorMultitrackAangle


def read_tracks(track_paths, tstep):
    return \
        [mtc.TrackData.from_csv(path,force_tstep=tstep) \
         for path in track_paths]

def plot_compare_1d(tacks_list, codec_function, size = None):
    n_tracks = len(tacks_list)
    fig, axes = plt.subplots(2*n_tracks, 1, sharex=True, sharey=True)

    for idx, xy in enumerate(tacks_list):
        xy_compressed = codec_function(xy)
        xy_diff = xy_compressed-xy

        ax_x = axes[idx*2]
        ax_x.plot(xy[:,0], marker='o', color='k')
        ax_x.plot(xy_compressed[:,0], marker='o', color='b')
        ax_x.set_ylabel('X_%d' % idx)
        ax_x_delta = ax_x.twinx()
        ax_x_delta.plot(xy_diff[:,0], marker='o', color='r')

        ax_y = axes[idx*2+1]
        ax_y.plot(xy[:,1], marker='o', color='k')
        ax_y.plot(xy_compressed[:,1], marker='o', color='b')
        ax_y.set_ylabel('Y_%d' % idx)
        ax_y_delta = ax_y.twinx()
        ax_y_delta.plot(xy_diff[:,1], marker='o', color='r')

    for ax in axes:
        ax.grid(True)
        ax.yaxis.set_label_coords(-0.05, 0.5)

    if not size:
        size = (14,6*len(tacks_list))

    fig.set_size_inches(size)
    fig.tight_layout()
    return fig

def plot_compare_2d(tacks_list, codec_function, size = None):
    n_tracks = len(tacks_list)
    fig, axes = plt.subplots(n_tracks, 1)

    for idx, xy in enumerate(tacks_list):
        xy_compressed = codec_function(xy)
        maxerr = np.max(np.linalg.norm(xy_compressed-xy, axis=1))

        ax_xy = axes[idx]
        ax_xy.invert_yaxis()
        ax_xy.plot(xy[:,0], xy[:,1], marker='o', color='k')
        ax_xy.plot(xy_compressed[:,0], xy_compressed[:,1], marker='o', color='b')
        ax_xy.set_xlabel('XY_%d, max err=%f' % (idx, maxerr))

    for ax in axes:
        ax.grid(True)
        ax.yaxis.set_label_coords(-0.05, 0.5)
        ax.set_aspect('equal', adjustable='datalim')

    if not size:
        size = (7,7*len(tacks_list))

    fig.set_size_inches(size)
    fig.tight_layout()
    return fig

def optimize_ddpcm(
    tracks,
    niter = None,
    step_update_interval = None,
    temperature = None,
    initial_guess = None,
    hdf_fpath = None,
    dataset_name = None,
    plot_path = None,
    error_function = None):

    if niter is None:
        niter = 15000

    if error_function is None:
        error_function = get_error_multitrack_maxnorm

    if step_update_interval is None:
        step_update_interval = 25

    if temperature is None:
        temperature = 0.5

    diff_table_size = \
        16 \
        if initial_guess is None \
        else initial_guess.shape[0]

    opt_history = \
        DiffTableOptimizerHistory(
            niter, \
            ndiffs=diff_table_size)

    opt_diff, err, opt_result =\
        DDPCMDiffTableOptimizer(
            tracks,
            niter = niter,
            interval = step_update_interval,
            temperature = temperature,
            initial_guess = initial_guess,
            callback = opt_history,
            err_function = error_function)\
        .get_optimal_difftable()

    opt_history.postprocess()

    if hdf_fpath is not None:
        opt_history.save_to_hdf(\
           hdf_fpath,
           dataset_name = dataset_name,
           opt_diff = opt_diff,
           err = err,
           niter = niter,
           temperature = temperature,
           interval = step_update_interval)

    if plot_path is not None:
        fig = opt_history.plot()
        fig.savefig(plot_path)

    return (opt_diff, err, opt_result)

def print_usage():
    print(
        """usage:
       ddpcm_track_encoder.py command

       command is one of
       --optmize-ddpcm
           --track-paths       - semicolon separated list of tracks
           --diff-table        - initial guess
           --opt-dataset-name  - dataset name for saving optimization
                                 results to hdf
           --opt-ntiter        - number of iterations of basin-hopping
                                 method
           --opt-interval      - step updating interval of basin-hopping
                                 method
           --opt-temperature   - temperature for basin-hopping method
           --hdf-fpath         - path to hdf file where optimization
                                 results will be saved
           --plot-opt-history  - path to plot of optimization convergence
                                 history
           --tstep-ms          - timestep in ms (default is 8)
           --errf-max-distance - error function is max distance from
                                 original track point to decoded track
                                 point (this is default error function)
           --errf-max-dist-angle - error function is sum of max distance
                                 from original track point to
                                 decoded track point and normalized
                                 squared angle error with coefficient
                                 equal to parameter argument

       --list-hdf
           --hdf-fpath        - hdf file to read optimization variants from

       --compress-ddpcm         encodes several tracks using DDPCM with
                                user-specified or default difference table
           --track-paths           - semicolon separated list of tracks
           --diff-table            - diff table (a list of integers with
                                    any separators)
           --plot-comparison-1d
           --plot-comparison-2d
           --track-c-header-folder - track data and diff table
                                     will be saved in this folder as
                                     c header files
           --track-csv-folder      - original and decoded track point
                                     coordinates will be saved in this
                                     folder
           --tstep-ms              - timestep in ms (default is 8)""")

def _throw_if_multiple_commands(current_cmd, new_cmd):
    if current_cmd is not None:
        print("ERROR: more than one command specified: %s, %s" % (urrent_cmd, new_cmd))
        print_usage()
        sys.exit(3)

def _parse_arguments():
    try:
        opts, args = \
            getopt.getopt(
                sys.argv[1:],
                "h",
                ["help=",
                 "optmize-ddpcm",
                 "compress-ddpcm",
                 "list-hdf",
                 "track-paths=",
                 "opt-dataset-name=",
                 "opt-ntiter=",
                 "tstep-ms=",
                 "opt-interval=",
                 "opt-temperature=",
                 "hdf-fpath=",
                 "plot-opt-history=",
                 "plot-compression=",
                 "plot-comparison-1d=",
                 "plot-comparison-2d=",
                 "diff-table=",
                 "track-c-header-folder=",
                 "track-csv-folder=",
                 "errf-max-dist",
                 "errf-max-dist-angle="])
    except getopt.GetoptError as ex:
        print("ERROR: Invalid parameters")
        print(ex)
        print_usage()
        sys.exit(2)

    command = None
    dataset_name = None
    ntiter = None
    interval = None
    hdf_fpath = None
    opt_history_plot_fpath = None
    temperature = None
    track_paths = None
    comparison_1d_path = None
    comparison_2d_path = None
    diff_table = None
    tstep = 8
    track_c_header_folder = None
    errf = None
    errf_angle_coef = 2.0
    track_output_csv_folder = None

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print_usage()
            sys.exit()
        elif opt in ('--optmize-ddpcm',):
            _throw_if_multiple_commands(command, opt)
            command = "optmize-ddpcm"
        elif opt in ('--compress-ddpcm',):
            _throw_if_multiple_commands(command, opt)
            command = "compress-ddpcm"
        elif opt in ('--list-hdf',):
            _throw_if_multiple_commands(command, opt)
            command = "list-hdf"
        elif opt in ('--opt-dataset-name',):
            dataset_name = arg
        elif opt in ('--opt-ntiter',):
            ntiter = int(arg)
        elif opt in ('--opt-interval',):
            interval = int(arg)
        elif opt in ('--hdf-fpath',):
            hdf_fpath = arg
        elif opt in ('--errf-max-dist',):
            if errf is not None:
                print("ERROR: multiple error functions specified")
                sys.exit(4)
            errf = 'errf-max-dist'
        elif opt in ('--errf-max-dist-angle',):
            if errf is not None:
                print("ERROR: multiple error functions specified")
                sys.exit(4)
            errf_angle_coef = float(arg)
            errf = 'errf-max-dist-angle'
        elif opt in ('--plot-opt-history',):
            opt_history_plot_fpath = arg
        elif opt in ('--opt-temperature',):
            temperature = float(arg)
        elif opt in ('--plot-comparison-1d',):
            comparison_1d_path = arg
        elif opt in ('--plot-comparison-2d',):
            comparison_2d_path = arg
        elif opt in ('--tstep-ms',):
            tstep = int(arg)
        elif opt in ('--track-paths',):
            track_paths = [x.strip() for x in arg.strip().split(";") if x]
            for fpath in track_paths:
                print(f"{fpath}\n")
                if not os.path.isfile(fpath):
                    print("ERROR: track file '%s' not found" % fpath)
                    sys.exit(4)
        elif opt in ('--diff-table',):
            diff_table_str = \
                [x.strip() for x in re.compile("[^\d\-\.]+").split(arg) if x]
            diff_table = \
                np.array(
                    [int(x) for x in diff_table_str],
                    dtype=np.int8)
        elif opt in ('--track-c-header-folder',):
            track_c_header_folder = arg
            if not os.path.isdir(track_c_header_folder):
                print(
                    "ERROR: track header output path '%s' not found" %\
                     track_c_header_folder)
                sys.exit(4)
        elif opt in ('--track-csv-folder',):
            track_output_csv_folder = arg
            if not os.path.isdir(track_output_csv_folder):
                print(
                    "ERROR: track csv output path '%s' not found" %\
                     track_c_header_folder)
                sys.exit(4)
        else:
            print("ERROR: unsupported argument "+opt)
            print_usage()
            sys.exit(3)

    if not errf:
        errf = 'errf-max-dist'


    if not command:
        print("ERROR: command not specified")
        print_usage()
        sys.exit(3)

    return \
        (command,
         track_paths,
         dataset_name,
         diff_table,
         ntiter,
         interval,
         temperature,
         hdf_fpath,
         tstep,
         track_c_header_folder,
         opt_history_plot_fpath,
         comparison_1d_path,
         comparison_2d_path,
         errf,
         errf_angle_coef,
         track_output_csv_folder)


if __name__ == "__main__":

    (command,
     track_paths,
     dataset_name,
     diff_table,
     ntiter,
     interval,
     temperature,
     hdf_fpath,
     tstep,
     track_c_header_folder,
     opt_history_plot_fpath,
     comparison_1d_path,
     comparison_2d_path,
     errf,
     errf_angle_coef,
     track_output_csv_folder) = _parse_arguments()

    if command == 'optmize-ddpcm':
      error_function = None
      if(errf=='errf-max-dist'):
        error_function = get_error_multitrack_maxnorm
      elif(errf=='errf-max-dist-angle'):
        error_function = ErrorMultitrackAangle(errf_angle_coef)
      else:
        print("ERROR: unsupported error function '%s'" % errf)
        sys.exit(3)

      tracks = read_tracks(track_paths, tstep)
      opt_diff, err, opt_result = \
        optimize_ddpcm(
            [track.xyu for track in tracks],
            niter = ntiter,
            step_update_interval = interval,
            temperature = temperature,
            initial_guess = diff_table,
            hdf_fpath = hdf_fpath,
            dataset_name = dataset_name,
            plot_path = opt_history_plot_fpath,
            error_function = error_function)
      print("\noptimal diff table:")
      print(opt_diff)
      print("\nmin error:")
      print(err)
    elif command == 'list-hdf':
      dataset_names, errs, diffs = \
        DiffTableOptimizerHistory.get_hdf_variants_list(hdf_fpath)
      for name, err, diff in zip(dataset_names, errs, diffs):
            print("name = '%s'" % name)
            print("err=%6.4f, diff=%s" % (err, ",".join(["%4i"%x for x in diff])))
    elif command == 'compress-ddpcm':
      tracks = read_tracks(track_paths, tstep)

      for i, (track_path, track) in enumerate(zip(track_paths, tracks)):
            print(f"\n ({i}) {track_path}")
            track.print_info()

      tracks_xyu = [track.xyu for track in tracks]

      ddpcm = \
        DDPCM(diff_table) \
        if diff_table is not None else \
        DDPCM(reflect_differences(DIFFERENCES_16))

      print("\ndiff table:")
      print(ddpcm.diff_table)

      codec_function = \
        lambda xy: ddpcm.decode(\
            ddpcm.encode(xy, symbols_dtype=np.uint8),
            first_delta=xy[1,:]-xy[0,:])

      max_err = \
        get_error_multitrack_maxnorm(tracks_xyu, codec_function)
      print("Max error = %f" % max_err)

      if comparison_1d_path is not None:
        fig = plot_compare_1d(tracks_xyu, codec_function)
        fig.savefig(comparison_1d_path)
      if comparison_2d_path is not None:
        fig = plot_compare_2d(tracks_xyu, codec_function)
        fig.savefig(comparison_2d_path)

      if track_c_header_folder is not None:
        header_fnames = []
        track_names = []
        for track_path, track in zip(track_paths, tracks):
            track_fname = \
                os.path.basename(\
                    os.path.splitext(track_path)[0])
            header_fname = track_fname+'.h'
            header_fnames.append(header_fname)
            track_h_path = \
                os.path.join(track_c_header_folder,header_fname)

            track_name = track_fname.upper()
            track_names.append(track_name)

            write_ddpcm_encoded_track_c_data_header(
                ddpcm,
                track.xyu,
                track_h_path,
                track_name,
                tstep)

        write_diff_table_c_data_header(
            ddpcm.diff_table,
            os.path.join(track_c_header_folder,'ddpcm_difftable.h'))

        write_tracks_library_c_data_header(
            header_fnames,
            track_names,
            os.path.join(track_c_header_folder,'tracks-library.h'))

      if track_output_csv_folder is not None:
        for track_path, track in zip(track_paths, tracks):
            track_fname = \
                os.path.basename(\
                    os.path.splitext(track_path)[0])
            track_csv_path = \
                os.path.join(
                    track_output_csv_folder,
                    'interpolated_'+track_fname+'.csv')

            track.xy_decoded = codec_function(track.xyu)
            track.write_csv_data(track_csv_path)

            track_ddxy_csv_path = \
                os.path.join(
                    track_output_csv_folder,
                    'ddxy_'+track_fname+'.csv')
            symbols = ddpcm.encode(track.xyu, symbols_dtype=np.uint8)
            ddxy_data = np.empty((symbols.shape[0],6), dtype=float)
            #print((symbols.shape[0],5))
            #print(ddxy_data.shape)
            #print(track.tu.shape)
            ddxy_data[:,0]   = track.tu[:symbols.shape[0]]
            ddxy_data[:,1:3] = symbols
            ddxy_data[:,3:5] = ddpcm.diff_table[symbols]
            ddxy_data[:,5] = np.linalg.norm(ddxy_data[:,3:5], axis=1)
            np.savetxt(\
                track_ddxy_csv_path,
                ddxy_data,
                delimiter=',',
                header='t,codex,codey,ddx,ddy,dda',
                fmt=('%d','%d','%d','%d', '%d', '%4.1f'),
                comments='')


    else:
      print("ERROR: command %s is not implemented" % command)
      print_usage()
      sys.exit(3)
