#!/usr/bin/env bash

OUTPUT_DIR='../chosen-tracks-data'

tracks_list=(\
    '../mouse-tracks/misc/track061.csv' \
    '../mouse-tracks/misc/track070.csv' \
    '../mouse-tracks/misc/track071.csv' \
    #'../mouse-tracks/misc/track072.csv' \
    '../mouse-tracks/misc/track073.csv' \
    #'../mouse-tracks/misc/track080.csv' \
    #'../mouse-tracks/misc/track082.csv' \
    #'../mouse-tracks/misc/track085.csv' \
    #'../mouse-tracks/misc/track087.csv' \
    #'../mouse-tracks/misc/track088.csv' \
    '../mouse-tracks/desktop-small-circle/track151.csv' \
    '../mouse-tracks/desktop-small-circle/track153.csv' \
    '../mouse-tracks/desktop-small-circle/track155.csv' \
    '../mouse-tracks/desktop-small-circle/track156.csv' \
    '../mouse-tracks/desktop-small-circle/track157.csv' \
    '../mouse-tracks/desktop-small-circle/track158.csv' \
    '../mouse-tracks/desktop-2022/track177.csv' \
    '../mouse-tracks/desktop-2022/track178.csv' \
    '../mouse-tracks/desktop-2022/track179.csv' \
    '../mouse-tracks/desktop-2022/track180.csv' \
    '../mouse-tracks/desktop-2022/track186.csv' \
    '../mouse-tracks/desktop-2022/track190.csv' \
    '../mouse-tracks/desktop-2022/track191.csv' \
    '../mouse-tracks/desktop-2022/track192.csv' \
    '../mouse-tracks/desktop-2022/track194.csv' \
    '../mouse-tracks/desktop-2022/track195.csv' \
    '../mouse-tracks/desktop-2022/track196.csv' \
    '../mouse-tracks/desktop-2022/track197.csv' \
    '../mouse-tracks/desktop-2022/track198.csv' \
    '../mouse-tracks/desktop-2022/track199.csv' \
    '../mouse-tracks/desktop-2022/track200.csv' \
    '../mouse-tracks/desktop-2022/track201.csv' \
    #'../mouse-tracks/desktop-2022/track202.csv' \
    #'../mouse-tracks/desktop-2022/track203.csv' \
    #'../mouse-tracks/desktop-2022/track204.csv' \
    #'../mouse-tracks/desktop-2022/track205.csv' \
    '../mouse-tracks/desktop-2022/track206.csv' \
    #'../mouse-tracks/desktop-2022/track207.csv' \
    #'../mouse-tracks/desktop-2022/track208.csv' \
    '../mouse-tracks/desktop-2022/track209.csv' \
    '../mouse-tracks/desktop-2022/track210.csv' \
    #'../mouse-tracks/desktop-2022/track211.csv' \
    #'../mouse-tracks/desktop-2022/track212.csv' \
    '../mouse-tracks/desktop-2022/track213.csv' \
    #'../mouse-tracks/desktop-2022/track214.csv' \
    #'../mouse-tracks/desktop-2022/track215.csv' \
    '../mouse-tracks/desktop-2022/track216.csv' \
    #'../mouse-tracks/desktop-2022/track217.csv' \
    #'../mouse-tracks/desktop-2022/track218.csv' \
    #'../mouse-tracks/desktop-2022/track219.csv' \
    #'../mouse-tracks/desktop-2022/track220.csv' \
    '../mouse-tracks/desktop-2022/track221.csv' \
    #'../mouse-tracks/desktop-2022/track222.csv' \
    '../mouse-tracks/desktop-2022/track223.csv' \
    '../mouse-tracks/desktop-2022/track224.csv' \
    '../mouse-tracks/desktop-2022/track225.csv' \
    '../mouse-tracks/desktop-2022/track226.csv' \
    '../mouse-tracks/desktop-2022/track227.csv' \
    #'../mouse-tracks/desktop-2022/track228.csv' \
    '../mouse-tracks/desktop-2022/track229.csv' \
    #'../mouse-tracks/desktop-2022/track230.csv' \
)
tracks_list_combined=$(printf '%s;' "${tracks_list[@]}")

echo $tracks_list_combined

./ddpcm_track_encoder.py \
    --compress-ddpcm \
    --tstep-ms 8 \
    --plot-comparison-1d "${OUTPUT_DIR}/ddpcm_err1d.png" \
    --plot-comparison-2d "${OUTPUT_DIR}/ddpcm_err2d.png" \
    --track-c-header-folder "${OUTPUT_DIR}" \
    --track-csv-folder "${OUTPUT_DIR}" \
    --track-paths "$tracks_list_combined"
