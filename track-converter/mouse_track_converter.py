#!/usr/bin/env python
import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import chevron
import getopt
import sys

import matplotlib.pyplot as plt
import matplotlib.cm as cm



def read_track(track_path, skip_start=0, skip_last=0):
    track = pd.read_csv(
            track_path,
            names = ['EventType','Timestamp','x', 'y'])
    # ignoring mouse click events
    track = track[track.EventType=='MV']
    if skip_start or skip_last:
      track = track.iloc[skip_start:-skip_last]
    t = track.Timestamp.to_numpy()
    xy = track[['x','y']].to_numpy()
    t -= t[0]
    xy[:,0] -= xy[0,0]
    xy[:,1] -= xy[0,1]
    return (t, xy)

def get_steps(dt):
    # https://stackoverflow.com/questions/10741346/numpy-most-efficient-frequency-counts-for-unique-values-in-an-array
    y = np.bincount(dt)
    ii = np.nonzero(y)[0]
    return np.vstack((ii, y[ii])).T

def get_step(dt):
    # https://www.geeksforgeeks.org/find-the-most-frequent-value-in-a-numpy-array/
    return np.bincount(dt).argmax()

def equalize_steps(t, xy, step = None):
    t_dedup, xy_dedup = _deduplicate_time(t, xy)
    return _equalize_steps(t_dedup, xy_dedup, step=step)

def _deduplicate_time(t, xy):
    idx = np.argwhere(t[1:]==t[:-1])
    if idx.shape[0] >0:
        return \
            (np.delete(t, idx),
             np.delete(xy, idx, axis=0))
    else:
        return t, xy


def _equalize_steps(t, xy, step = None):
    if not step:
      dt = t[1:]-t[:-1]
      step = get_step(dt)

    tmin = np.float(t[0])
    tmax = np.float(t[-1])
    nsteps = \
        np.floor((tmax-tmin)/step)\
          .astype(np.int)

    t_new = \
        tmin + np.arange(0,nsteps+1)*step

    xy_new = \
        np.empty(
            (t_new.shape[0],2),
             dtype=np.int)
    xy_new[:,0] = _interpolate(t, xy[:,0], t_new)
    xy_new[:,1] = _interpolate(t, xy[:,1], t_new)

    t_new_int = np.rint(t_new).astype(np.int64)

    return (t_new_int, xy_new)

def _interpolate(t_old, x, t_new):
  interpolant = \
    interp1d(
      t_old,x,
      kind='cubic',
      assume_sorted=True,
      copy=False)
  return \
      np.rint(
          interpolant(t_new))\
        .astype(np.int)

def get_dxy(xy):
    return xy[1:,:]-xy[:-1,:]

def get_clipped_dxy(xy, d_min=-128, d_max=127):
    return \
        np.clip(
            xy[1:,:]-xy[:-1,:],
            d_min, d_max)

def dxy_to_xy(dxy, x0=0, y0=0):
    xy = np.empty((dxy.shape[0]+1, 2), dtype=np.int64)
    xy[0,:] = [x0,y0]
    xy[1:,:] = np.cumsum(dxy, axis=0)
    xy[1:,0] += x0
    xy[1:,1] += y0
    return xy

def get_speed_acceleration(t, xy):
    dt = t[1:]-t[:-1]
    dxy = xy[1:,:]-xy[:-1,:]
    vxy = dxy/dt[:,np.newaxis].astype(np.float)
    dvxy = vxy[1:,:]-vxy[:-1,:]
    axy = dvxy/dt[:-1,np.newaxis].astype(np.float)
    return (vxy, axy)

def convert_to_cdata(
    delta_t: int,
    xy,
    dtype: str = 'char',
    array_name: str = 'MOUSE_TRACK',
    timestep_macro = 'MOUSE_TRACK_DT_MS'):

    lines = []

def plot_compare_xy(
    t1, xy1, t2, xy2, vxy2,
    title: str = None,
    xlabel='X [pixels]',
    ylabel='Y [pixels]'):

    fig, axs = plt.subplots(2, 2, sharex=True, gridspec_kw={'width_ratios': [2, 1]})
    gs = axs[1, 0].get_gridspec()

    ax1 = axs[0,0]
    ax1.plot(t1, xy1[:,0], marker='o')
    ax1.plot(t2, xy2[:,0], marker='.')
    ax1.set_ylabel(xlabel)
    ax1.grid(True)

    ax2 = axs[1,0]
    ax2.plot(t1, xy1[:,1], marker='o')
    ax2.plot(t2, xy2[:,1], marker='.')
    ax2.set_ylabel(ylabel)
    ax2.grid(True)
    ax2.set_xlabel('time [ms]')

    for ax in axs[:, 1]:
        ax.remove()
    ax3 = fig.add_subplot(gs[:, 1])
    v = np.sum(vxy2**2,axis=1)**0.5
    colors = np.ones(xy2.shape[0])
    colors[:-1] = (v-np.min(v))/np.max(v)

    ax3.scatter(xy2[:,0],-xy2[:,1], c=colors, cmap=cm.jet)
    ax3.plot(xy2[:,0],-xy2[:,1], color='k', linestyle=':')

    prop = dict(arrowstyle="-|>,head_width=0.4,head_length=0.8",
            shrinkA=0,shrinkB=0,color='r')

    ax3.annotate(
        "",
        xy=(xy2[-1,0],-xy2[-1,1]),
        xytext=(xy2[0,0],-xy2[0,1]),
        arrowprops=prop)

    ax3.grid(True)

    if title:
        ax1.set_title(title, fontsize=15)
    fig.set_size_inches((20,6))

    return fig

C_DATA_TEMPLATE = \
"""{{=<% %>=}}
#ifndef HEADER_GUARD_<%track_name%>
#define HEADER_GUARD_<%track_name%>

const unsigned int <%track_name%>_TSTEP_MS = <%tstep%>;

const int <%track_name%>_TOTAL_DELTA[2] = {<%total_delta_x%>, <%total_delta_y%>};

const signed char <%track_name%>_DXY[<%track_length%>][2] = {
    <%#dxy%>
    {<%x%>, <%y%>},
    <%/dxy%>
};
#endif
"""

ARDUINESE_DATA_TEMPLATE = \
"""{{=<% %>=}}
#ifndef HEADER_GUARD_<%track_name%>
#define HEADER_GUARD_<%track_name%>

#define <%track_name%>_TSTEP_MS <%tstep%>
#define <%track_name%>_TOTAL_DELTA_X <%total_delta_x%>
#define <%track_name%>_TOTAL_DELTA_Y <%total_delta_y%>

const PROGMEM int8_t <%track_name%>_DXY[<%track_length%>][2] = {
    <%#dxy%>
    {<%x%>, <%y%>},
    <%/dxy%>
};

#define <%track_name%>_LENGTH (sizeof(<%track_name%>_DXY)/sizeof(<%track_name%>_DXY[0]))

#endif
"""

def write_c_data_header(
    fname,
    track_name,
    tstep,
    dxy,
    arduino=False):

  track_length = dxy.shape[0]
  total_delta = np.sum(dxy,0)
  with open(fname, 'w') as f:
    f.write(
      chevron.render(
        ARDUINESE_DATA_TEMPLATE if arduino else C_DATA_TEMPLATE,
        {"track_name": track_name,
         "tstep": tstep,
         "track_length": track_length,
         "total_delta_x": str(total_delta[0]),
         "total_delta_y": str(total_delta[1]),
         "dxy": [{"x": str(dxy[i,0]),
                  "y": str(dxy[i,1])}
                  for i in range(track_length)]}))

class TrackData:
    def __init__(self,
         t, xy, tstep, tu, xyu, dxyu_clipped,
         xyu_clipped, vxy_clipped, axy_clipped,
         xy_decoded = None,
         ddxy_decoded = None):
        """
        t            - original track timestamps
        xy           - original track point coordinates
        tstep        - timestep after timestamp regularization
        tu           - uniform timestamps after time regularization
        xyu          - track points after time regularization
        dxyu_clipped - track point deltas
                       clipped to -128..127 (int8) range
        xyu_clipped  - track point coordinates after clipping
                       deltas
        vxy_clipped  - velocities in px/ms for track with clipped
                       deltas
        axy_clipped  - accelerations in px/ms^2 for track with clipped
                       deltas
        xy_decoded   - track points on uniform grid after decoding
                       (if any),
        """

        self.t = t
        self.xy = xy
        self.tstep = tstep
        self.tu = tu
        self.xyu = xyu
        self.dxyu_clipped = dxyu_clipped
        self.xyu_clipped = xyu_clipped
        self.vxy_clipped = vxy_clipped
        self.axy_clipped = axy_clipped
        self.xy_decoded = xy_decoded
        self.ddxy_decoded = ddxy_decoded

    @staticmethod
    def from_csv(csv_path, force_tstep=None):
        print(csv_path)
        t, xy = read_track(csv_path)
        tstep = force_tstep if force_tstep else get_step(t[1:]-t[:-1])
        tu, xyu = equalize_steps(t, xy, step=tstep)
        dxyu_clipped = get_clipped_dxy(xyu)
        xyu_clipped = dxy_to_xy(dxyu_clipped)
        vxy_clipped, axy_clipped = get_speed_acceleration(tu, xyu_clipped)

        return TrackData(
            t, xy,
            tstep, tu, xyu,
            dxyu_clipped, xyu_clipped,
            vxy_clipped, axy_clipped)

    def print_info(self):
        n = self.tu.shape[0]
        print("timestep = %d ms, length = %d ms" % (self.tstep, n *self.tstep))
        print("N points = %d -> %d bytes" % (n, n*2))
        print("DeltaXY = (%d, %d)" % tuple(self.xyu_clipped[-1,:]))
        dxyu = get_dxy(self.xyu)
        ddxyu = get_dxy(dxyu)
        max_dxyu = np.max(np.abs(dxyu), axis=0)
        max_ddxyu = np.max(np.abs(ddxyu), axis=0)
        print(f"Max(dxu)  = {max_dxyu[0]}")
        print(f"Max(ddxu) = {max_ddxyu[0]}")
        print(f"Max(dyu)  = {max_dxyu[1]}")
        print(f"Max(ddyu) = {max_ddxyu[1]}")

    def plot_compare_xy(self):
        n=self.tu.shape[0]
        return plot_compare_xy(
            self.t,self.xy,
            self.tu,
            self.xyu_clipped,
            self.vxy_clipped,
            title='timestep = %d ms, length = %d ms / %d points / %d bytes, dxy = (%d, %d)' % (
                self.tstep, n *self.tstep, n, 2*n,
                self.xyu_clipped[-1,0]-self.xyu_clipped[0,0],
                self.xyu_clipped[-1,1]-self.xyu_clipped[0,1]))

    def write_c_data_header(self, fname, track_name, arduino=False):
        write_c_data_header(
            fname,
            track_name,
            self.tstep,
            self.dxyu_clipped,
            arduino=arduino)

    def write_csv_data(self, fname):
        data = np.empty((self.tu.shape[0],9))
        data[:,0] = self.tu
        data[:,1:3] = self.xyu
        data[:-1,3] = np.linalg.norm(self.vxy_clipped, axis=1)
        data[-1,3] = 0
        data[:,4:6] = \
            self.xyu_clipped \
            if self.xy_decoded is None \
            else self.xy_decoded
        data[:,6:8] = data[:,4:6]-data[:,1:3]
        data[:,8] = np.linalg.norm(data[:,6:8], axis=1)
        np.savetxt(\
            fname,
            data,
            delimiter=',',
            header='t,x,y,v,xdecoded,ydecoded,xerror,yerror,abserror',
            fmt=('%d','%d','%d','%f','%d','%d','%d','%d','%4.1f'),
            comments='')


def print_usage():
    print(
        """usage:
       mouse_track_converter.py -i|--input-file <path>
           [--output-plot <path>] [--output-header <path>]
           [--output-csv <path>] [-n|--track-name <name>]
           [--arduino] [--force-step <tstepms>]

       -i|--input-file  input file path (csv)
       --output-header output file path (c header)
       --output-plot output file path (plot x(t), y(t) and xy plot)
       --output-csv output file path (csv)
       --force-step is timestep that should be used instead of
         automatically calculated value for interpolation
       -n|--track-name track name used for variables and constants inside output header
       --arduino generate arduino header with coordinates stored in program memory
       --putput-plot""")


def _parse_arguments():
    try:
        opts, args = \
            getopt.getopt(
                sys.argv[1:],
                "hi:o:n:",
                ["help=",
                 "input-file=",
                 "output-header=",
                 "output-plot=",
                 "output-csv=",
                 "track-name=",
                 "force-tstep=",
                 "arduino"])
    except getopt.GetoptError as ex:
        print("ERROR: Invalid parameters")
        print(ex)
        print_usage()
        sys.exit(2)

    arduino = False
    input_file = None
    output_header = None
    output_plot = None
    output_csv = None
    track_name = 'TRACK'
    force_tstep = None

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print_usage()
            sys.exit()
        elif opt in ('-i', '--input-file'):
            input_file = arg
        elif opt in ('--output-header'):
            output_header = arg
        elif opt in ('--output-plot'):
            output_plot = arg
        elif opt in ('--output-csv'):
            output_csv = arg
        elif opt in ('--force-tstep'):
            force_tstep = int(arg)
        elif opt in ('-n', '--track-name'):
            track_name = arg
        elif opt in ('--arduino',):
            arduino = True
        else:
            print("ERROR: unsupported argument "+opt)
            print_usage()
            sys.exit(3)

    if not input_file:
        print("ERROR: input file not specified")
        print_usage()
        sys.exit(3)

    if not output_header \
       and not output_plot\
       and not output_csv:
        print("ERROR: output file not specified")
        print_usage()
        sys.exit(3)

    return (input_file, output_header,
            track_name, arduino, output_plot,
            output_csv, force_tstep)


if __name__ == "__main__":

    (input_file,
     output_header,
     track_name,
     arduino,
     output_plot,
     output_csv,
     force_tstep) = _parse_arguments()

    track_data = \
        TrackData.from_csv(
            input_file,
            force_tstep=force_tstep)

    if output_csv:
        track_data.write_csv_data(
            output_csv)

    if output_header:
        track_data.write_c_data_header(
            output_header,
            track_name,
            arduino = arduino)

    if output_plot:
        fig = track_data.plot_compare_xy()
        fig.savefig(output_plot)
