#!/usr/bin/env bash

tracks_list=$(ls ../mouse-tracker/tracks/desktop-small-circle/track*.csv | awk -vORS=';' '{print $1}')

mkdir -p 'ddpcm-optimization2'
mkdir -p 'ddpcm-optimization2/track_data_headers'
mkdir -p 'ddpcm-optimization2/track_data_headers_opt1'
mkdir -p 'ddpcm-optimization2/track_data_headers_opt2'


./ddpcm_track_encoder.py \
    --compress-ddpcm \
    --plot-comparison-1d ddpcm_err1d.png \
    --plot-comparison-2d ddpcm_err2d.png \
    --track-c-header-folder 'ddpcm-optimization2/track_data_headers/' \
    --track-paths "$tracks_list"

./ddpcm_track_encoder.py \
    --compress-ddpcm \
    --diff-table "-125,-114, -54, -22, -20, -16, -11,  -5,   0,   5,  10,  17,  48,  82,  86, 127" \
    --plot-comparison-1d ddpcm_opt1_err1d.png \
    --plot-comparison-2d ddpcm_opt1_err2d.png \
    --track-c-header-folder 'ddpcm-optimization2/track_data_headers_opt1/' \
    --track-paths "$tracks_list"

./ddpcm_track_encoder.py \
    --compress-ddpcm \
    --diff-table "-126, -99, -69, -60, -15, -12,  -7,  -4,   1,   6,   7,  12,  17,  29, 112, 114" \
    --plot-comparison-1d ddpcm_opt2_err1d.png \
    --plot-comparison-2d ddpcm_opt2_err2d.png \
    --track-c-header-folder 'ddpcm-optimization2/track_data_headers_opt2/' \
    --track-paths "$tracks_list"