#!/usr/bin/env bash

tracks_list=$(ls ../mouse-tracker/tracks/desktop-small-circle/track*.csv | awk -vORS=';' '{print $1}')

mkdir -p 'ddpcm-optimization2'
HDF_PATH='ddpcm-optimization2/ddpcmv2.hdf5'

for in in $(seq 1 30); do
   ./ddpcm_track_encoder.py \
        --optmize-ddpcm \
        --opt-dataset-name
        --opt-ntiter 15000 \
        --hdf-fpath $HDF_PATH \
        --plot-opt-history test-opt-history.png \
        --track-paths "$tracks_list" 
done        

./ddpcm_track_encoder.py \
    --list-hdf \
    --hdf-fpath $HDF_PATH
