import numpy as np

from .exceptions import *
from .utils import *

from .octosymmetricgrid import \
    OctoSymmetricGrid



class CompositeOctoSymmetricGrid:
    def __init__(self, distances, subgrids):                           
        if len(distances) != len(subgrids):
            raise DistancesAndChainsHaveDifferentLength(
                "Number of distances %d does not match number of chains %d" %\
                (len(distances), list_of_point_chains45.shape[0]))
            
        self.__subgrids = subgrids
        self.__distances = distances
        
    @staticmethod
    def full_grid(
        distances,
        grid_adjuster: None):
        
        subgrids = [OctoSymmetricGrid.full_grid(d) for d in distances]
        
        if grid_adjuster:
            grid_adjuster(subgrids)
        
        return CompositeOctoSymmetricGrid(distances, subgrids)
    
    @staticmethod        
    def decimated_full_grid(
        distances,
        grid_reduction_coefficients,
        grid_adjuster: None):
        
        if len(distances) != len(grid_reduction_coefficients):
            raise DistancesAndCorefficientsHaveDifferentLengths()
        
        subgrids = \
            [OctoSymmetricGrid.decimated_full_grid(d, cf) \
             for d, cf in zip(distances, grid_reduction_coefficients)]
                    
        if grid_adjuster:
            grid_adjuster(subgrids)
            
        return CompositeOctoSymmetricGrid(distances, subgrids)
    
    @staticmethod
    def from_npoints(
        distances,
        number_of_points,
        grid_adjuster: None):
        
        if len(distances) != len(number_of_points):
            raise DistancesAndNumbersOfPointsHaveDifferentLengths()
        
        subgrids = \
            [OctoSymmetricGrid.from_npoints(d, n) \
             for d, n in zip(distances, number_of_points)]
                            
        if grid_adjuster:
            grid_adjuster(subgrids)
        
        return CompositeOctoSymmetricGrid(distances, subgrids)
    
    @property
    def subgrid_npoints45(self):
        return (subgrid.n_points45 for subgrid in self.__subgrids)
    
    @property
    def subgrid_npoints90(self):
        return (subgrid.n_points90 for subgrid in self.__subgrids)
    
    @property
    def subgrid_npoints360(self):
        return (subgrid.n_points360 for subgrid in self.__subgrids)
    
    @property
    def n_points45(self):
        return sum(self.subgrid_npoints45)
    
    @property
    def n_points90(self):
        return sum(self.subgrid_npoints90)
    
    @property
    def n_points360(self):
        return sum(self.subgrid_npoints360)
                
    def get_subgrids45(self):
        return self.__subgrids
    
    def get_subgrids90(self):
        return [g45.get_grid90() for g45 in self.__subgrids]
    
    def get_subgrids360(self):
        return [g45.get_grid360() for g45 in self.__subgrids]
    
    def plot_grid45(self, ax, **scatter_args):
        for subgrid in self.__subgrids:
            subgrid.plot_grid45(ax, **scatter_args)
            
    def plot_grid90(self, ax, **scatter_args):
        for subgrid in self.__subgrids:
            subgrid.plot_grid90(ax, **scatter_args)
                        
    def plot_grid360(self, ax, **scatter_args):
        for subgrid in self.__subgrids:
            subgrid.plot_grid360(ax, **scatter_args)
    
    def print_npoint_report(self):
        print("N points by distance (45):\n"+ 
          "\n".join((\
             "%4d - %4d" % (d,n_points) \
             for d, n_points in \
                 zip(self.__distances, self.subgrid_npoints45))))
        print("Total N points (45)  = %d\n" % self.n_points45)

        print("N points by distance (90):\n"+ 
              "\n".join((\
                 "%4d - %4d" % (d,n_points) \
                 for d, n_points in \
                     zip(self.__distances, self.subgrid_npoints90))))
        print("Total N points (90)  = %d\n" % self.n_points90)

        print("N points by distance (360):\n"+ 
              "\n".join((\
                 "%4d - %4d" % (d,n_points) \
                 for d, n_points in \
                 zip(self.__distances, self.subgrid_npoints360))))
        print("Total N points (360) = %d" % self.n_points360)