class InvalidPointForGrid45(Exception):
    pass

class DistancesAndChainsHaveDifferentLength(Exception):
    pass

class DistancesAndCorefficientsHaveDifferentLengths(Exception):
    pass

class DistancesAndNumbersOfPointsHaveDifferentLengths(Exception):
    pass
