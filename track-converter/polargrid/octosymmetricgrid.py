import numpy as np

from .exceptions import *
from .utils import *

class OctoSymmetricGrid:
    def __init__(self, points45):
        if np.any(\
                np.logical_or(
                    np.logical_or(
                        points45[:,1]>points45[:,0], 
                        points45[:,0]<0),
                    points45[:,1]<0)):
            raise InvalidPointForGrid45(
                "Some of the points are outside 0..45 deg sector")
        
        if(len(points45.shape)!=2):
            raise InvalidPointForGrid45(
                "points array should be of shape (Npoints, Ndimensions)")
                    
        self.__points45 = points45
        
    @property
    def n_points45(self):
        return self.__points45.shape[0]
    
    @property
    def n_points90(self):
        return self.n_points45*2-2
    
    @property
    def n_points360(self):
        return (self.n_points45*2-2)*4
    
    @property
    def grid45(self):
        return self.__points45
    
    def get_grid90(self):
        return expand_g45_to_g90(self.__points45)
    
    def get_grid360(self):
        return expand_g45_to_g360(self.__points45) 
    
    def plot_grid45(self, ax, **scatter_args):        
        ax.scatter(
            self.__points45[:,0], 
            self.__points45[:,1],
            **scatter_args)
            
    def plot_grid90(self, ax, **scatter_args):
        points90 = self.get_grid90()
        ax.scatter(
            points90[:,0], 
            points90[:,1],
            **scatter_args)
                        
    def plot_grid360(self, ax, **scatter_args):
        points360 = self.get_grid360()
        ax.scatter(
            points360[:,0], 
            points360[:,1],
            **scatter_args)
        
    @staticmethod
    def full_grid(distance):                
        return OctoSymmetricGrid(get_full_grid45(distance))
        
    @staticmethod
    def decimated_full_grid(
        distance,
        reduction_coefficient):
        
        g45_full = get_full_grid45(distance)
        g45_compressed = g45_full[::reduction_coefficient]
        
        return OctoSymmetricGrid(g45_compressed)
    
    @staticmethod
    def from_npoints(
        distance,
        npoints):
        
        angles = np.linspace(0,np.pi/4.0, npoints)        
        points45 = get_unique_grid_points_from_angles(
                    distance, 
                    angles)
        
        return OctoSymmetricGrid(points45)
                                        