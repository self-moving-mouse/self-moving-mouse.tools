import numpy as np

def configure_gridplot90(ax, fig, distances, size=(20,20)):
    max_distance = distances[-1]
    minor_ticks = np.arange(0,max_distance+1)
    major_ticks = [0]+distances    
    ax.set_xticks(minor_ticks, minor=True)    
    ax.set_yticks(minor_ticks, minor=True)
    ax.xaxis.set_tick_params(width=3)
    ax.yaxis.set_tick_params(width=3)
    ax.set_yticks(major_ticks)
    ax.set_xticks(major_ticks)
    
    ax.set_xlim(-0.5,max_distance+1)
    ax.set_ylim(-0.5,max_distance+1)    
    
    ax.grid(True, which='major',linestyle='-', color='gray')
    ax.grid(True, which='minor',linestyle=':', color='gray')
    fig.set_size_inches(size)    
    
def configure_gridplot360(ax, fig, distances, size=(20,20)):
    max_distance = distances[-1]
    minor_ticks = np.arange(-1-max_distance,max_distance+1)
    major_ticks = [-x for x in reversed(distances)]+[0]+distances    
    ax.set_xticks(minor_ticks, minor=True)    
    ax.set_yticks(minor_ticks, minor=True)
    ax.xaxis.set_tick_params(width=3)
    ax.yaxis.set_tick_params(width=3)
    ax.set_yticks(major_ticks)
    ax.set_xticks(major_ticks)  
    
    ax.set_xlim(-1-max_distance,max_distance+1)
    ax.set_ylim(-1-max_distance,max_distance+1)        
    
    ax.grid(True, which='major',linestyle='-', color='gray')
    ax.grid(True, which='minor',linestyle=':', color='gray')
    fig.set_size_inches(size) 