import numpy as np

def __get_min_grid_angle(d):
    #      Y
    #      ^
    #      |
    #  d+1 |     * A
    #      |
    #   d  |     * B
    #      | 
    #      |       
    #      +---------------> X
    #    O       1
    # AOB is the smallest possible angle between two points
    # on a circle
    # AO1 + OA1 = 90
    # AOB = A01 - BO1
    #
    # AOB = 90 - OA1 - BO1 =
    #     = 90 - atan(1/d+1) - atan(d/1)
    return np.pi/2 - np.arctan2(1,d+1) - np.arctan2(d,1)

def get_unique_grid_points_from_angles(distance, angles):
    """
    Computes nearest cartesian integer pixel coordinates 
    from polar coordinates: distance (the same for all points)
    and angles list (in radians).
    Filters out matching points and returns resulting point list 
    of shape (n,2) with int16 datatype
    """
    grid = np.empty((angles.shape[0],2), dtype=np.int16)
    grid[:,0] = np.round(distance*np.cos(angles))
    grid[:,1] = np.round(distance*np.sin(angles))
    
    # for small distances and/or small radii 
    # adjacent angles can produce equal points
    _, argunique = np.unique(grid, axis=0, return_index=True)        
    return grid[np.sort(argunique),:]

def get_full_grid45(distance):
    da = __get_min_grid_angle(distance)/10
    test_angles = np.arange(0.0, np.pi/4, step=da)
    
    return get_unique_grid_points_from_angles(
            distance, 
            test_angles)

def __get_reflection_mask(grid45):
    reflection_mask = \
        np.logical_and(
            grid45[:,0]!=0,
            grid45[:,1]<grid45[:,1])
    
    n_reflected = np.count_nonzero(reflection_mask)
    
    return (reflection_mask, n_reflected)

def expand_g45_to_g90(grid45):      
    n45 = grid45.shape[0]
    
    reflection_mask, n_reflected = \
        __get_reflection_mask(grid45)
    
    grid90 = np.empty((n45+n_reflected,2), dtype=grid45.dtype)
    grid90[:n45, :] = grid45
    grid90[n45:, 0] = grid45[reflection_mask, 1]
    grid90[n45:, 1] = grid45[reflection_mask, 0]
    return grid90  

def expand_g45_to_g360(grid45):
    n45 = grid45.shape[0]
    
    reflection_mask, n_reflected = \
        __get_reflection_mask(grid45)
    
    n90 = n45+n_reflected
    grid360 = np.empty((n90*4,2), dtype=grid45.dtype)

    grid360[:n45, :] = grid45

    grid360[n45:n90, 0] = grid45[reflection_mask, 1]
    grid360[n45:n90, 1] = grid45[reflection_mask, 0]

    grid360[n90:2*n90,0] = -grid360[:n90, 1]
    grid360[n90:2*n90,1] =  grid360[:n90, 0]

    grid360[2*n90:,0] = -grid360[:n90*2, 0]
    grid360[2*n90:,1] = -grid360[:n90*2, 1]
    
    return grid360 