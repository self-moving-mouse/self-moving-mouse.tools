from .exceptions import InvalidDataShape

from .dpcm import DPCM
from .dpcm_multidim import DPCM_Multidimentional_FullTable
from .ddpcm import DDPCM
from .ddpcm_multidim import DDPCM_Multidimentional_FullTable

from .adaptive_common import \
    DiffTableOptimizerHistory, \
    get_error_singletrack_maxnorm, \
    get_error_multitrack_maxnorm, \
    get_angles, \
    get_error_singletrack_dist_angle, \
    ErrorMultitrackAangle, \
    reflect_differences, \
    DIFFERENCES_8POWERS_OF_2, \
    DIFFERENCES_16

from .adaptive_dpcm import \
    DPCMDiffTableOptimizer

from .adaptive_ddpcm import \
    DDPCMDiffTableOptimizer

from .ddpcm_exporter import \
    write_diff_table_c_data_header, \
    write_ddpcm_encoded_track_c_data_header, \
    write_dpcm_encoded_track_c_data_header, \
    write_tracks_library_c_data_header
    