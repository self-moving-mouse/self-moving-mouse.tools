import sys
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt
import h5py

DIFFERENCES_8POWERS_OF_2 = [1,2,4,8,16,32,64,128]

DIFFERENCES_16 = [1,2,3,4,6,8,12,16]

def reflect_differences(distances):
    return np.array([-d for d in reversed(distances)]+distances, dtype=np.int)

def get_error_singletrack_maxnorm(xy, codec_function):    
    xy_decoded = codec_function(xy)    
    return np.max(\
              np.linalg.norm(
                xy.astype(np.float64)-xy_decoded.astype(np.float64), 
                axis=1))

def get_error_multitrack_maxnorm(tacks_list, codec_function):
    errors = np.array([\
        get_error_singletrack_maxnorm(xy, codec_function) \
        for xy in tacks_list])        
    
    return np.max(errors)

def _get_angles(xy_normalized):
    xy_dot = np.sum(xy_normalized[:-1,:]*xy_normalized[1:,:], axis=1)
    xy_cross = np.cross(xy_normalized[:-1,:],xy_normalized[1:,:], axis=1)
    angles = np.arcsin(xy_cross)
    dm_mask = xy_dot<0
    dmcp_mask = np.logical_and(dm_mask, xy_cross>0)
    dmcm_mask = np.logical_and(dm_mask, xy_cross<0)    
    angles[dmcp_mask] = np.pi-angles[dmcp_mask]
    angles[dmcm_mask] = -np.pi-angles[dmcm_mask]
    
    return angles  

_C0 = 1.0/(2*np.pi)**2

def get_angles(xy, xy_decoded):    
    xy_decoded_delta = xy_decoded[1:,:]-xy_decoded[:-1,:]
    xy_decoded_delta_norm = np.linalg.norm(xy_decoded_delta, axis=1)
    
    xy_delta = (xy[1:,:]-xy[:-1,:]) 
    xy_delta_norm = np.linalg.norm(xy_delta, axis=1)
    nonzero_mask = \
        np.logical_and(
            xy_decoded_delta_norm>=10, 
            xy_delta_norm>=10)
    
    xy_decoded_delta_normalized = xy_decoded_delta[nonzero_mask,:]
    xy_decoded_delta_normalized[:,0]/=xy_decoded_delta_norm[nonzero_mask]
    xy_decoded_delta_normalized[:,1]/=xy_decoded_delta_norm[nonzero_mask]
    
    xy_delta_normalized = xy_delta[nonzero_mask,:]
    xy_delta_normalized[:,0]/=xy_delta_norm[nonzero_mask]
    xy_delta_normalized[:,1]/=xy_delta_norm[nonzero_mask]
    
    
    xy_angles = _get_angles(xy_delta_normalized)
    xy_decoded_angles = _get_angles(xy_decoded_delta_normalized)
    
    return xy_angles, xy_decoded_angles 
    
    

def get_error_singletrack_dist_angle(xy, codec_function, angle_coeff=2.0):
    xy_double = xy.astype(np.double)
    
    xy_decoded = codec_function(xy).astype(np.float64)     
    xy_angles, xy_decoded_angles = get_angles(xy_double, xy_decoded)
    
    return \
        angle_coeff*_C0*(np.max((xy_decoded_angles-xy_angles)**2/(2*np.pi)**2))+\
        np.max(\
              np.linalg.norm(
                xy_double-xy_decoded, 
                axis=1))       

class ErrorMultitrackAangle:
    def __init__(self, angle_coeff=2.0):
        self.__angle_coeff = angle_coeff
    
    def __call__(self, tacks_list, codec_function):
        errors = np.array([\
            get_error_singletrack_dist_angle(
                xy, 
                codec_function, 
                angle_coeff=self.__angle_coeff) \
            for xy in tacks_list])        
    
        return np.max(errors)        


class _AdaptiveDPCM_TakeStep:
    def __init__(self, initial_step_size=64):        
        self.stepsize = int(initial_step_size)
        self.__small_changes = np.array([-1, 0, 1], dtype=np.int)
    
        
    def __call__(self, x):
        if self.stepsize >= 1.0:
            effective_stepsize = np.clip(int(self.stepsize), 1,127)
        
            x += np.random.randint(
                    -effective_stepsize,
                    effective_stepsize+1,
                    size=x.shape)
            np.clip(x, -127, 127, out=x)
            x.sort()
            return x
        else:
            prob_no_change = (1.0-2.0/3.0*self.stepsize)
            prob_change = 1.0/3.0*self.stepsize
            probabilities = np.array([prob_change, prob_no_change, prob_change])
            x += np.random.choice(
                self.__small_changes, 
                size = x.shape, 
                p = [prob_change, prob_no_change, prob_change])
            np.clip(x, -127, 127, out=x)
            x.sort()
            return x
    
class DiffTableOptimizerHistory:
    def __init__(self, 
                 niter = None, 
                 ndiffs = 16, 
                 history_diff = None, 
                 history_err=None, 
                 histroy_step=None):        
        self.iter = 0
        self.history_diff = \
            history_diff \
            if history_diff is not None else \
            np.empty((niter+1,ndiffs), dtype=np.int)
        self.history_err = \
            history_err \
            if history_err is not None else \
            np.empty(niter+1)
        self.histroy_step = \
            histroy_step \
            if histroy_step is not None else \
            np.empty(niter+1)       
    
    def __call__(self, diff, err, accept, step_size):
        self.history_diff[self.iter,:] = diff
        self.history_err[self.iter] = err
        self.histroy_step[self.iter] = step_size
        self.iter+=1
        if self.iter % 100 ==0:
            print('\riteration = %d, step = %f' % (self.iter, step_size), file=sys.stderr, end="")
            sys.stderr.flush()
            
    def postprocess(self):
        self.history_diff = \
            np.sort(
                self.history_diff,
                axis = 1)            
        
    def plot(self, size = (15, 20)):        
        fig, (ax_diff, ax_err, ax_step) = \
            plt.subplots(3, 1, sharex=True)
    
        ax_err.plot(self.history_err)    
        ax_err.plot(np.minimum.accumulate(self.history_err))    
        ax_err.set_yscale('log')
        ax_err.grid(True, which='both')

        for idx in range(self.history_diff.shape[1]):
            ax_diff.plot(self.history_diff[:,idx])
        ax_diff.grid(True, which='both')

        ax_step.plot(self.histroy_step)    
        ax_step.grid(True, which='both')

        fig.set_size_inches(size)   
        return fig
        
    def save_to_hdf(self, 
            fname: str, 
            dataset_name = None,
            opt_diff = None, 
            err: float = None, 
            niter: int = None,
            temperature: float = None, 
            interval: int = None):
        
        if dataset_name is None:
            dataset_name = datetime.now().strftime("%Y%m%d%H%M%S")
        
        opt_diff_idx = np.argmin(self.history_err)
        if opt_diff is None:
            opt_diff = self.history_diff[opt_diff_idx,:]
        
        if err is None:
            err = self.history_err[opt_diff_idx]
            
        if niter is None:
            niter = self.history_err.shape[0] - 1                       
        
        with h5py.File(fname, "a") as f:
            f.create_dataset(dataset_name+"/parameters/niter", data= niter)
            if interval is not None:
                f.create_dataset(dataset_name+"/parameters/interval", data= 25)
            if temperature is not None:
                f.create_dataset(dataset_name+"/parameters/temperature", data= 0.5)
            f.create_dataset(dataset_name+"/optimum/err", data= err)
            f.create_dataset(dataset_name+"/optimum/diff", data=opt_diff)
            f.create_dataset(dataset_name+"/history/diff", data=self.history_diff)
            f.create_dataset(dataset_name+"/history/err", data=self.history_err)
            f.create_dataset(dataset_name+"/history/step", data=self.histroy_step)
            
    @staticmethod
    def get_hdf_variants_list(fname: str):
        errs = []
        diffs = []
        dataset_names = []
        with h5py.File(fname, "r") as f:
            for dataset_name in f.keys():
                dataset_names.append(dataset_name)
                errs.append(f[dataset_name]['optimum']['err'][()])
                diffs.append(f[dataset_name]['optimum']['diff'][()])
        sorted_idx = np.argsort(errs)
        errs = [errs[i] for i in sorted_idx]
        diffs = [diffs[i] for i in sorted_idx]
        dataset_names = [dataset_names[i] for i in sorted_idx]
        
        return (dataset_names, errs, diffs)
        
            
    @staticmethod
    def from_hdf(fname: str, dataset_name = None):
        with h5py.File(fname, "r") as f:
            dataset_name = sorted(f.keys())[0]
            history_diff = f[dataset_name]['history']['diff'][()]
            history_err = f[dataset_name]['history']['err'][()]
            history_step = f[dataset_name]['history']['step'][()]

            if 'parameters' in f[dataset_name]:
                temperature = f[dataset_name]['parameters']['temperature'][()]
                interval = f[dataset_name]['parameters']['interval'][()]
                niter = f[dataset_name]['parameters']['niter'][()]
            else:
                temperature = None
                interval = None
                niter = self.history_err.shape[0]-1

            opt_err = f[dataset_name]['optimum']['err'][()]
            opt_diff = f[dataset_name]['optimum']['diff'][()]
            return (\
               DiffTableOptimizerHistory(
                   history_diff = history_diff,
                   history_err = history_err,
                   history_step = history_step),
               (dataset_name,
                opt_diff,
                opt_err,
                niter,
                temperature,
                interval)) 

  