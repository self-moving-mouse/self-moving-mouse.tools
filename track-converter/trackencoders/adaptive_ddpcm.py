import sys

import numpy as np
from scipy.optimize import basinhopping,OptimizeResult

from .adaptive_common import \
    _AdaptiveDPCM_TakeStep, \
    reflect_differences, \
    DIFFERENCES_8POWERS_OF_2

from .ddpcm import DDPCM

class DDPCMDiffTableOptimizer:    
    def __init__(self, 
                 tracks, 
                 initial_guess = None,
                 initial_step_size: int = 64, 
                 niter: int = 10000,
                 interval: int = 25,
                 temperature: float = 0.5,
                 callback = None,
                 err_function = None):
        """
        callback - a function with arguments (diff, err, accept, step_size)
        """
        self.__errfunction = \
             get_error_multitrack_maxnorm \
             if err_function is None \
             else err_function
        
        self.__tracks = tracks
        self.__niter = niter
        self.__initial_guess = \
            reflect_differences(DIFFERENCES_8POWERS_OF_2)\
            if initial_guess is None \
            else initial_guess 
        self.__interval = interval
        self.__temperature = temperature
        self.__initial_step_size = initial_step_size
        self.__stepper = \
            _AdaptiveDPCM_TakeStep(
                initial_step_size = initial_step_size)
        self.__callback = callback
        
    def __bh_callback(self, x, f, accept):
        if self.__callback is not None:
            self.__callback(x, f, accept, self.__stepper.stepsize)
            
    def __dummy_local_minimizer(self, fun, x0, args, **kwargs):
        """
        Does not do anything. This way 
        local search stage of basinhopping
        is excluded
        """
        res = OptimizeResult()
        res.x = x0
        res.fun = fun(x0)
        res.success = True
        res.status = 0
        res.nit = 1
        res.maxcv = 0
        return res
    
    def __bh_target(self, differences):
        ddpcm = DDPCM(differences)
        return self.__errfunction(
            self.__tracks, 
            lambda x: ddpcm.decode(
                    ddpcm.encode(x, symbols_dtype=np.uint8),
                    first_delta=x[1,:]-x[0,:]))
    
    def get_optimal_difftable(self):
        opt_result = \
            basinhopping(
                self.__bh_target,
                niter = self.__niter,
                T = self.__temperature,
                interval = self.__interval,
                x0 = self.__initial_guess,
                stepsize = self.__initial_step_size,
                take_step = self.__stepper,
                minimizer_kwargs= {'method':self.__dummy_local_minimizer},
                callback = self.__bh_callback)
                        
        return \
            (np.sort(\
                 np.unique(
                    opt_result.x.astype(self.__initial_guess.dtype))), 
             opt_result.fun, 
             opt_result)

