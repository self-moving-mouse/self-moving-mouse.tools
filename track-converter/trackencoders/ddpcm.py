import numpy as np

from .exceptions import InvalidDataShape

class DDPCM:
    """
    DPCM based lossy track encoder
    that chooses accelerations from user provided list
    so that decoded track error is minimized.
    Multidimensional imput "channels" (x and y for mouse tracks)
    are processed independently.
    
    For mouse tracks this provides the same compression as
    DPCM but smaller error
    and much more smooth and subjectively "realistic" tracks.
    """
    def __init__(self, diff_table):
        diff_array = np.array(diff_table, dtype=np.int)
        if len(diff_array.shape)!=1:
            raise InvalidDataShape(
                "Differences table should be a 1d array")
        self.diff_table = diff_array

    def __encode_1d(self, input_buf, dtype):        
        output_buf = \
            np.empty(
                input_buf.shape[0]-2,
                dtype)
        
        # predicion     = x_{i-1}
        # prediction_dx = \Delta x_{i-2}
        
        prediction = input_buf[1]
        prediction_dx  = input_buf[1]-input_buf[0]        
        for i, input_sample in enumerate(input_buf[2:]):            
            predictions = prediction + prediction_dx + self.diff_table
            abs_error = np.abs(predictions - input_sample)
            diff_index = np.argmin(abs_error)
            output_buf[i] = diff_index
            prediction_dx += self.diff_table[diff_index]
            prediction += prediction_dx
            
        return output_buf
            
    def __decode_1d(self, 
                    input_buf, 
                    first_sample, 
                    first_delta, 
                    dtype):
        
        differences = self.diff_table[input_buf]                
        result = np.empty(input_buf.shape[0]+2, dtype=dtype)
        result[0] = first_sample
        result[1] = first_sample+first_delta                
        result[2:] = \
            result[1] + \
            np.cumsum(
                first_delta +
                np.cumsum(differences, dtype=dtype))
        return result
        
    def encode(self, input_buf, symbols_dtype = np.uint):                
        input_dim = len(input_buf.shape)
        if input_dim>2:
            raise InvalidDataShape(
                "Input data should be 1d or 2d array "+
                "of shape (NSamples,SampleSize)")
        if input_dim==1:            
            return self.__encode_1d(input_buf, symbols_dtype)
        else:
            return np.stack(
                [self.__encode_1d(input_buf[:,i], symbols_dtype)\
                 for i in range(input_buf.shape[1])],
                axis=1)

    def decode(self, 
               input_buf, 
               first_sample=None, 
               first_delta=None, 
               result_dtype=np.double):
        
        input_dim = len(input_buf.shape)
        if input_dim>2:
            raise InvalidDataShape(
                "Input data should be 1d or 2d array of "+
                "shape (NSamples,SampleSize)")                                        
        if input_dim==1:               
                                    
            return self.__decode_1d(
                input_buf, 
                first_sample if first_sample else 0,
                first_delta if first_delta else 0,
                result_dtype)
        else:
            if first_sample is not None \
               and first_sample.shape != (input_buf.shape[1], ):
                
               raise InvalidDataShape(
                   "first_sample shape %s does not match sample size %d" % \
                   (first_sample.shape, input_buf.shape[1]))
            
            if first_delta is not None \
               and first_delta.shape != (input_buf.shape[1], ):
                
               raise InvalidDataShape(
                   "first_delta shape %s does not match sample size %d" % \
                   (first_delta.shape, input_buf.shape[1]))
            
            return np.stack(
                [self.__decode_1d(
                    input_buf[:,i], 
                    0 if first_sample is None else first_sample[i],
                    0 if first_delta is None else first_delta[i],
                    result_dtype) \
                 for i in range(input_buf.shape[1])],
                axis=1)     