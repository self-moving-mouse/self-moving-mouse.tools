import numpy as np
import chevron

ARDUINESE_DDPCM_DIFF_TABLE_TEMPLATE = \
"""{{=<% %>=}}
#ifndef HEADER_GUARD_DDPCM_DIFFTABLE
#define HEADER_GUARD_DDPCM_DIFFTABLE

const PROGMEM int8_t DDPCM_DELTA[<%diff_table_length%>] = {
    <%#difftable%>
    <%.%>,
    <%/difftable%>
};

#define DDPCM_DELTA_LENGTH (sizeof(DDPCM_DELTA))

#endif
"""

def write_diff_table_c_data_header(
    diff_table,
    fname):
    
    with open(fname, 'w') as f:
        f.write(
           chevron.render(
               ARDUINESE_DDPCM_DIFF_TABLE_TEMPLATE,
               {"diff_table_length": diff_table.shape[0],                
                "difftable": list(diff_table)}))

C_HEADER_TRACKS_LIBRARY_TEMPLATE = \
"""{{=<% %>=}}
#ifndef HEADER_GUARD_TRACKS_LIBRARY
#define HEADER_GUARD_TRACKS_LIBRARY

<%#headerfnames%>
#include "<%.%>"
<%/headerfnames%>

static PGM_VOID_P tracks[] = {
<%#tracknames%>    
    &<%.%>_DDPCM,
<%/tracknames%>  
    };

static const uint8_t track_sizes[] = {
<%#tracknames%>    
    <%.%>_LENGTH,
<%/tracknames%>
  };

#define N_TRACKS (sizeof(track_sizes))  

#endif
"""

def write_tracks_library_c_data_header(
    header_fnames,
    track_names,
    fname):
    with open(fname, 'w') as f:
        f.write(
           chevron.render(
               C_HEADER_TRACKS_LIBRARY_TEMPLATE,
               {"headerfnames": header_fnames,
                "tracknames": track_names}))               
        
        
ARDUINESE_DDPCM_TRACK_DATA_TEMPLATE = \
"""{{=<% %>=}}
#ifndef HEADER_GUARD_<%track_name%>
#define HEADER_GUARD_<%track_name%>

#define <%track_name%>_TSTEP_MS <%tstep%>

// first two bytes contain x and y components of initial velocity
// in pixels/step (should be cast to int8 type)
// all the following bytes contain 2 4-bit codes
// of x acceleration (low bits) and y acceleration (high bits) 
// in pixels/step^2 
const PROGMEM uint8_t <%track_name%>_DDPCM[<%track_length%>] = {
    <%first_delta_x%>,
    <%first_delta_y%>,
    <%#encoded_track%>
    <%.%>,
    <%/encoded_track%>
};

#define <%track_name%>_LENGTH (sizeof(<%track_name%>_DDPCM)-2)

#endif
"""     

def write_ddpcm_encoded_track_c_data_header(
    ddpcm_encoder,
    track_xy,
    fname,
    track_name,
    tstep):
    
    encoded_track = \
        ddpcm_encoder.encode(
            track_xy, 
            symbols_dtype=np.uint8)
    packed_encoded_track = \
        np.bitwise_or(
            encoded_track[:,0], 
            encoded_track[:,1]<<4)
    
    first_delta = \
        (track_xy[1,:]-track_xy[0,:])\
        .astype(np.uint8)

    with open(fname, 'w') as f:
        f.write(
           chevron.render(
               ARDUINESE_DDPCM_TRACK_DATA_TEMPLATE,
               {"track_name": track_name,
                "tstep": tstep,
                "track_length": packed_encoded_track.shape[0]+2,
                "first_delta_x": "0x%02x"%first_delta[0],
                "first_delta_y": "0x%02x"%first_delta[1],
                "encoded_track": ["0x%02x"%x for x in packed_encoded_track]}))
        
ARDUINESE_DPCM_TRACK_DATA_TEMPLATE = \
"""{{=<% %>=}}
#ifndef HEADER_GUARD_<%track_name%>
#define HEADER_GUARD_<%track_name%>

#define <%track_name%>_TSTEP_MS <%tstep%>

// Each byte contains 2 4-bit codes
// of x velocity (low bits) and y velocity (high bits) 
// in pixels/step 
const PROGMEM uint8_t <%track_name%>_DPCM[<%track_length%>] = {
    <%#encoded_track%>
    <%.%>,
    <%/encoded_track%>
};

#define <%track_name%>_LENGTH (sizeof(<%track_name%>_DPCM))

#endif
"""         


def write_dpcm_encoded_track_c_data_header(
    dpcm_encoder,
    track_xy,
    fname,
    track_name,
    tstep):
    
    encoded_track = \
        dpcm_encoder.encode(
            track, 
            symbols_dtype=np.uint8)
    packed_encoded_track = \
        np.bitwise_or(
            encoded_track[:,0], 
            encoded_track[:,1]<<4)    

    with open(fname, 'w') as f:
        f.write(
           chevron.render(
               ARDUINESE_DPCM_TRACK_DATA_TEMPLATE,
               {"track_name": track_name,
                "tstep": tstep,
                "track_length": packed_encoded_track.shape[0],
                "encoded_track": ["0x%02x"%x for x in packed_encoded_track]}))