import numpy as np

from .exceptions import InvalidDataShape

class DDPCM_Multidimentional_FullTable:
    """
    DPCM based lossy track encoder
    that chooses accelerations from user provided list
    so that decoded track error is minimized.
    In contrast to "classic" DPCM differences (accelerations) table
    samples are multidimentional (have the same length
    as input samples).
    
    For mouse tracks this provides the same compression as
    DPCM_Multidimentional_FullTable but smaller error
    and much more smooth and subjectively "realistic" tracks.
    """
    def __init__(self, diff_table):
        if len(diff_table.shape)!=2:
            raise InvalidDataShape("Differences table should be a 2d array (Ndifferences, SampleSize)")
            
        self.diff_table = diff_table 
        
    @property
    def sample_size(self):
        return self.diff_table.shape[1]
        
    def __encode(self, input_buf, dtype):
        output_buf = \
            np.empty(
                input_buf.shape[0]-2, 
                dtype=dtype)
        
        prediction = np.copy(input_buf[0,:])
        prediction_delta = np.copy(input_buf[1,:]-input_buf[0,:])
        
        for i in range(2,input_buf.shape[0]):
            predictions = \
                prediction + \
                prediction_delta + \
                self.diff_table
            abs_error = np.linalg.norm(predictions - input_buf[i,:], axis=1)
            diff_index = np.argmin(abs_error)
            output_buf[i-2] = diff_index
            prediction_delta += self.diff_table[diff_index]
            prediction += prediction_delta
            
        return output_buf
    
    def __decode(self, input_buf, first_sample, first_delta, dtype):        
        result = \
            np.empty(
                (input_buf.shape[0]+2, self.sample_size), 
                dtype=dtype)
        result[0,:] = first_sample
        result[1,:] = first_sample+first_delta
        
        differences = self.diff_table[input_buf,:]        
        result[2:,:] = \
            first_sample + \
            np.cumsum(
                first_delta + \
                    np.cumsum(differences, axis=0, dtype=dtype),
                axis=0)
        
        return result
            
    def encode(self, input_buf, symbols_dtype = np.uint):
        if len(input_buf.shape)!=2:
            raise InvalidDataShape(
                "Input data should be 2d array of shape (NSamples,SampleSize)")
        if(input_buf.shape[1]!=self.sample_size):
            raise InvalidDataShape(
                "Input data sample size %d does not match differences table sample size %d" %\
                (input_buf.shape[1], self.sample_size))
                        
        return self.__encode(input_buf, symbols_dtype)
    
    
    def decode(self, input_buf, first_sample=None, first_delta=None, result_dtype=np.double):
        input_dim = len(input_buf.shape)
        if input_dim>1:
            raise InvalidDataShape("Input data should be 1d array") 
                
        if first_sample is not None \
           and first_sample.shape != (self.sample_size, ):

           raise InvalidDataShape(
               "first_sample shape %s does not match sample size %d" % \
               (first_sample.shape, self.sample_size))
            
        if first_delta is not None \
           and first_delta.shape != (self.sample_size, ):

           raise InvalidDataShape(
               "first_delta shape %s does not match sample size %d" % \
               (first_sample.shape, self.sample_size))
        
            
        return self.__decode(
            input_buf,
            first_sample \
                if first_sample is not None \
                else np.zeros(self.sample_size, dtype=result_dtype),
            first_delta \
                if first_delta is not None \
                else np.zeros(self.sample_size, dtype=result_dtype),
            result_dtype)
        
        