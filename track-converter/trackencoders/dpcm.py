import numpy as np

from .exceptions import InvalidDataShape

class DPCM:
    """
    This is a somewhat modified implementation of DPCM     
    taken from here https://abrightersun.de/blog/dpcm-analyzed.html
    Author: Maurice-Pascal Sonnemann
   
    Multidimensional imput "channels" (x and y for mouse tracks)
    are processed independently.
    """
    def __init__(self, diff_table):
        diff_array = np.array(diff_table)
        if len(diff_array.shape)!=1:
            raise InvalidDataShape("Differences table should be a 1d array")
        self.diff_table = diff_array

    def __encode_1d(self, input_buf, dtype):
        output_buf = \
            np.empty(
                input_buf.shape[0]-1,
                dtype)
        prediction = input_buf[0]
        for i, input_sample in enumerate(input_buf[1:]):
            predictions = prediction + self.diff_table
            abs_error = np.abs(predictions - input_sample)
            diff_index = np.argmin(abs_error)
            output_buf[i] = diff_index
            prediction += self.diff_table[diff_index]
        return output_buf
            
    def __decode_1d(self, input_buf, first_sample, dtype):
        differences = self.diff_table[input_buf]
        result = np.empty(input_buf.shape[0]+1, dtype=dtype)
        result[0] = first_sample
        np.cumsum(differences, dtype=dtype, out=result[1:]) 
        result[1:] += first_sample        
        return result
        
    def encode(self, input_buf, symbols_dtype = np.uint):                
        input_dim = len(input_buf.shape)
        if input_dim>2:
            raise InvalidDataShape("Input data should be 1d or 2d array of shape (N,2)")
        if input_dim==1:            
            return self.__encode_1d(input_buf, symbols_dtype)
        else:
            return np.stack(
                [self.__encode_1d(input_buf[:,i], symbols_dtype)\
                 for i in range(input_buf.shape[1])],
                axis=1)

    def decode(self, input_buf, first_sample=None, result_dtype=np.double):
        input_dim = len(input_buf.shape)
        if input_dim>2:
            raise InvalidDataShape("Input data should be 1d or 2d array of shape (N,2)")        
        if input_dim==1:            
            return self.__decode_1d(
                input_buf, 
                first_sample if first_sample else 0,
                result_dtype)
        else:
            return np.stack(
                [self.__decode_1d(
                    input_buf[:,i], 
                    first_sample[i] if first_sample else 0,
                    result_dtype) \
                 for i in range(input_buf.shape[1])],
                axis=1)   