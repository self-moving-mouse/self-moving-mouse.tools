import numpy as np

from .exceptions import InvalidDataShape

class DPCM_Multidimentional_FullTable:
    """
    Variant of DPCM for multidimensional imput.
    In contrast to "classic" DPCM differences (accelerations) table
    samples are multidimentional (have the same length
    as input samples).    
    At the cost of increased differences table size this 
    provides somewhat smoother and subjectively "realistic" results
    than classic DPCM that processes each input channel separately.    
    """
    def __init__(self, diff_table):
        if len(diff_table.shape)!=2:
            raise InvalidDataShape("Differences table should be a 2d array (Ndifferences, SampleSize)")
            
        self.diff_table = diff_table        
        
    def __encode(self, input_buf, dtype):        
        output_buf = \
            np.empty(
                input_buf.shape[0]-1, 
                dtype=dtype)
        
        prediction = np.copy(input_buf[0,:])
        for i in range(1,input_buf.shape[0]):
            predictions = prediction + self.diff_table
            abs_error = np.linalg.norm(predictions - input_buf[i,:], axis=1)
            diff_index = np.argmin(abs_error)
            output_buf[i-1] = diff_index
            prediction += self.diff_table[diff_index]
        return output_buf
            
    def __decode(self, input_buf, first_sample, dtype):
        differences = self.diff_table[input_buf]
        result = \
            np.empty(
                (input_buf.shape[0]+1, 
                 self.diff_table.shape[1]), 
                dtype=dtype)
        result[0,:] = first_sample
        np.cumsum(
            differences, 
            axis=0, 
            dtype=dtype, 
            out=result[1:,:])
        result[1:,:]+= first_sample
        return result

    def encode(self, input_buf, symbols_dtype = np.uint):
        if len(input_buf.shape)!=2:
            raise InvalidDataShape(
                "Input data should be 2d array of shape (NSamples,SampleSize)")
        if(input_buf.shape[1]!=self.diff_table.shape[1]):
            raise InvalidDataShape(
                "Input data sample size %d does not match differences table sample size %d" %\
                (input_buf.shape[1], self.diff_table.shape[1]))
                                                    
        return self.__encode(input_buf, symbols_dtype)

    def decode(self, input_buf, first_sample=None, result_dtype=np.double):
        if len(input_buf.shape)!=1:
            raise InvalidDataShape("Compressed data should be 1d array")
            
        if not first_sample:
            first_sample = \
                np.full(
                    self.diff_table.shape[1], 
                    0, 
                    dtype=result_dtype)
            
        return self.__decode(input_buf, first_sample, result_dtype)